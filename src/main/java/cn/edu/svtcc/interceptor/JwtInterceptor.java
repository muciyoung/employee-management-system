package cn.edu.svtcc.interceptor;

import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.utils.JwtUtil;
import cn.edu.svtcc.utils.Result;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@Component
@Slf4j
public class JwtInterceptor implements HandlerInterceptor {
    @Autowired
    private JwtUtil jwtUtil;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //从http请求头获取token
        String token = request.getHeader("X-Token");
        log.debug(request.getRequestURL()+"需要验证"+token);
        if (StringUtils.hasText(token)){
           try {
               //这里如果无法解析就会报错
               jwtUtil.parseToken(token, User.class);
               log.debug(request.getRequestURL()+"验证通过！");
           }catch (Exception e){
               log.debug(request.getRequestURL()+"验证异常！\n"+e.getMessage());
           }
            return true;
        }
        // 设置响应的内容类型为文本，带有 UTF-8 编码
        response.setContentType("text/plain; charset=UTF-8");
        response.getWriter().write(JSON.toJSONString(Result.error(405,"非法登陆！！")));
        return false;
    }
}