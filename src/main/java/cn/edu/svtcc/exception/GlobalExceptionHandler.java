package cn.edu.svtcc.exception;

import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全部异常处理
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/18
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = RuntimeException.class)
    public Result<T> handler(RuntimeException e){
        log.error("运行时异常：----",e);
        return Result.error(ResultCode.ERROR,e.getMessage());
    }
}
