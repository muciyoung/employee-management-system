package cn.edu.svtcc.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 账户被锁定数锁定异常
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/18
 */
public class UserCountLockException extends AuthenticationException {

    public UserCountLockException(String msg, Throwable t) {
        super(msg, t);
    }

    public UserCountLockException(String msg) {
        super(msg);
    }
}
