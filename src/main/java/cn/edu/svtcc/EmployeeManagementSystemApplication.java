package cn.edu.svtcc;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * 员工管理系统应用
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/09/27
 */
@Slf4j
@SpringBootApplication
@MapperScan("cn.edu.svtcc.*.mapper")
public class EmployeeManagementSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeManagementSystemApplication.class, args);
        log.info("---------项目启动成功！---------");
    }

}
