package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.Salary;
import cn.edu.svtcc.sys.mapper.SalaryMapper;
import cn.edu.svtcc.sys.service.ISalaryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
public class SalaryServiceImpl extends ServiceImpl<SalaryMapper, Salary> implements ISalaryService {

}
