package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.Docs;
import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.sys.mapper.DocsMapper;
import cn.edu.svtcc.sys.service.IDocsService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 文件表 服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-11-28
 */
@Service
public class DocsServiceImpl extends ServiceImpl<DocsMapper, Docs> implements IDocsService {

    @Value("${upload.path}") // 引入上传文件的存储路径
    private String fileUploadPath;

    @Resource
    private DocsMapper docsMapper;

    @Override
    public Result<?> deleteById(Long id) {
        if (this.docsMapper.deleteById(id)>0){
            return Result.success("删除成功！");
        }
        return Result.error(ResultCode.ERROR,"删除失败！");
    }

    @Override
    @Transactional
    public Result<?> deleteBatch(List<Long> ids) {
        if (this.docsMapper.deleteBatchIds(ids)>=0){
            return Result.success("批量删除成功！");
        }
        return Result.error(ResultCode.ERROR,"批量删除失败！");
    }

    @Override
    public Result<?> edit(Docs docs) {
        if (this.docsMapper.updateById(docs)>0){
            return Result.success("更新成功！");
        }
        return Result.error(ResultCode.ERROR,"更新失败！");
    }

    @Override
    public Result<?> findById(Long id) {
        Docs docs = this.docsMapper.selectById(id);
        return Result.success(docs);
    }

    @Override
    public Result<Map<String,Object>> list(Integer current, Integer size, String oldName, String uploader) {
        IPage<Docs> page = new Page<>(current, size);
        IPage<Docs> resultPage = this.docsMapper.selectFileByPage(page,oldName,uploader);
        //构建返回结果
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",resultPage.getTotal());
        map.put("rows",resultPage.getRecords());
        return Result.success(map);
    }

    @Override
    public Result<?> export(HttpServletResponse response) {
        //TODO:未完成
        return null;
    }

    @Override
    public Result<?> upload(MultipartFile file) throws IOException {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (Objects.isNull(user)){
            return Result.error(ResultCode.NO_AUTH,"未认证！");
        }
        if (Objects.isNull(file)){
            return Result.error(ResultCode.ERROR,"未选择上传文件 ！");
        }
        String originalFilename = file.getOriginalFilename(); // 获取文件的原名称
        String extName = FileUtil.extName(originalFilename); // 获取文件的后缀名
        String filename = IdUtil.fastSimpleUUID().substring(2, 22) + "." + extName; // 文件名

        // 获取文件的md5信息进行比对，判断是否需要重新写入文件
        String md5 = SecureUtil.md5(file.getInputStream());
        List<Docs> docsList = list(new QueryWrapper<Docs>().eq("md5", md5));
        // 如果存在就不上传，直接拿到文件名
        if (!docsList.isEmpty()){
            filename = docsList.get(0).getFileName();
        }else {
            File uploadFile = new File(fileUploadPath +File.separator+ filename);
            //不存在则将文件存储到磁盘
            file.transferTo(uploadFile);
        }
        // 将文件数据保存到数据库
        Docs docs = new Docs();
        docs.setFileName(filename);
        docs.setCreatedId(user.getUserId()); // 文件上传者
        docs.setType(extName);
        docs.setOldName(originalFilename);
        docs.setMd5(md5);
        docs.setSize(file.getSize() / 1024); // KB
        if (save(docs)) {
            return Result.success("文件上传成功！", docs);
        }
        return Result.error(ResultCode.ERROR,"上传失败！");
    }


    @Override
    public Result<?> download(String filename, HttpServletResponse response) throws IOException {
        // 通知浏览器以下载的方式打开
        response.addHeader("Content-Type", "application/octet-stream");
        response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename, String.valueOf(StandardCharsets.UTF_8)));
        // 通过文件流读取文件
        File downloadFile = new File(fileUploadPath +File.separator+ filename);
        OutputStream out = response.getOutputStream();
        // 读取文件的字节流
        out.write(FileUtil.readBytes(downloadFile));
        out.flush();
        out.close();
        return Result.success();
    }
}
