package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.Salary;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface ISalaryService extends IService<Salary> {

}
