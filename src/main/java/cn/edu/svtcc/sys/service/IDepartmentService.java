package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.Department;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface IDepartmentService extends IService<Department> {

    /**
     * 获取部门列表树。
     *
     * @return 包含部门信息的树形列表，每个节点是一个 {@link Department} 对象
     */
    List<Department> getAllDepartment();

    /**
     * 判断指定部门是否包含子部门。
     *
     * @param departmentId 部门编号
     * @return 如果存在子部门，则返回 true，否则返回 false
     */
    boolean hasChildrenDepartment(Long departmentId);

    /**
     * 分页查询部门列表。
     *
     * @param pageNo      当前页数
     * @param pageSize    每页记录数
     * @param department  包含查询条件的 Department 对象
     * @return 分页查询结果，包括部门信息的列表和分页信息
     */
    Result<Map<String, Object>> list(Long pageNo, Long pageSize, Department department);

    /**
     * 获取部门选择列表。
     *
     * @return 包含部门选择项的列表
     */
    Result<?> getDepartmentOptions();

    /**
     * 根据部门ID查询部门信息。
     *
     * @param departmentId 部门ID
     * @return 包含部门信息的查询结果
     */
    Result<?> findDepartmentById(Long departmentId);

    /**
     * 新增部门。
     *
     * @param department 包含新增部门信息的 Department 对象
     * @return 操作结果，包括成功或失败的信息
     */
    Result<?> addDepartment(Department department);

    /**
     * 更新部门信息。
     *
     * @param department 包含更新信息的 Department 对象
     * @return 操作结果，包括成功或失败的信息
     */
    Result<?> updateDepartment(Department department);

    /**
     * 根据部门ID删除部门。
     *
     * @param departmentId 部门ID
     * @return 操作结果，包括成功或失败的信息
     */
    Result<?> removeDepartmentById(Long departmentId);

}
