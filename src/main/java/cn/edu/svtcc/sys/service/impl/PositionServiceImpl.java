package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.Position;
import cn.edu.svtcc.sys.mapper.PositionMapper;
import cn.edu.svtcc.sys.service.IPositionService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
public class PositionServiceImpl extends ServiceImpl<PositionMapper, Position> implements IPositionService {

    @Resource
    private PositionMapper positionMapper;
    @Override
    public Result<?> findPositionById(Long positionId) {
        Position position = this.positionMapper.selectById(positionId);
        if (!Objects.isNull(position)){
            return Result.success("查询职位信息成功！",position);
        }
        return Result.error(ResultCode.ERROR,"查询职位信息失败！");
    }

    @Override
    public Result<?> getPositionOptions() {
        LambdaQueryWrapper<Position> wrapper = new LambdaQueryWrapper<Position>().select(Position::getPositionId, Position::getPositionName);
        List<Position> positionList = positionMapper.selectList(wrapper);
        if (!positionList.isEmpty()){
            return Result.success("职位列表获取成功！",positionList);
        }
        return Result.error(ResultCode.ERROR,"职位列表获取失败！");
    }

    @Override
    public Result<Map<String, Object>> list(Long pageNo, Long pageSize, Position position) {
        //构建分页对象
        Page<Position> page = new Page<>(pageNo, pageSize);

        //构建条件查询
        LambdaQueryWrapper<Position> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.hasLength(position.getPositionName()),Position::getPositionName,position.getPositionName());

        //根据ID降序排列
        wrapper.orderByDesc(Position::getPositionId);
        Page<Position> positionPage = this.positionMapper.selectPage(page, wrapper);

        //构建返回参数
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",positionPage.getRecords());
        map.put("total",positionPage.getTotal());

        return Result.success("查询成功！！",map);
    }

    @Override
    public Result<?> addPosition(Position position) {
        //参数校验
        if (Objects.isNull(position) || !StringUtils.hasLength(position.getPositionName())){
            return Result.error(ResultCode.ERROR,"职位信息不能为空 ！");
        }
        //查询是否存在该职位
        Position position1 = this.positionMapper.selectOne(new LambdaQueryWrapper<Position>().eq(Position::getPositionName, position.getPositionName()));
        if (!Objects.isNull(position1)){
            return Result.error(ResultCode.ERROR,"该职位已存在！");
        }
        //上述校验都通过则进行添加,先清除id，数据库自增
        position.setPositionId(null);
        if (this.positionMapper.insert(position)>0){
            return  Result.success("新增职位成功！");
        }
        return Result.error(ResultCode.ERROR,"新增职位失败");
    }
}
