package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.dto.EmployeeInfoDto;
import cn.edu.svtcc.sys.dto.EmployeeVo;
import cn.edu.svtcc.sys.entity.Employee;
import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface IEmployeeService extends IService<Employee> {

    Page<EmployeeInfoDto> getEmployeeByPage(Long pageNo, Long pageSize, Employee employee);

    Result<?> addEmployee(Employee employee);

    Result<?> updateEmployee(Employee employee);

    /**
     * 分配员工账号
     * @param employeeVo
     * @return
     */
    Result<?> assignEmployeeAccount(EmployeeVo employeeVo);
}
