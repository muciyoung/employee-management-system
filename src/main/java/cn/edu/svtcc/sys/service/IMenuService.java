package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.Menu;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface IMenuService extends IService<Menu> {

    /**
     * 获取所有菜单的列表。
     *
     * @return 包含所有菜单的列表
     */
    List<Menu> getAllMenu();

    /**
     * 根据用户ID查询当前登录用户所拥有的路由菜单列表。
     *
     * @param userId 用户ID
     * @return 当前登录用户的路由菜单列表
     */
    List<Menu> getMenuListByUserId(Long userId);

    /**
     * 根据菜单标题查询菜单是否存在。
     *
     * @param title 菜单标题
     * @return 如果存在匹配标题的菜单，则返回对应的 Menu 对象，否则返回 null
     */
    Menu getMenuByTitle(String title);

    /**
     * 判断指定菜单是否包含子菜单。
     *
     * @param menuId 菜单ID
     * @return 如果存在子菜单，则返回 true，否则返回 false
     */
    boolean hasChildrenMenu(Long menuId);

    /**
     * 新增菜单。
     *
     * @param menu 要新增的菜单对象
     * @return 操作结果，包括成功或失败的信息
     */
    Result<?> addMenu(Menu menu);

    /**
     * 更新菜单信息。
     *
     * @param menu 包含更新信息的 Menu 对象
     * @return 操作结果，包括成功或失败的信息
     */
    Result<?> updateMenu(Menu menu);

    /**
     * 根据菜单ID删除菜单。
     *
     * @param menuId 要删除的菜单ID
     * @return 操作结果，包括成功或失败的信息
     */
    Result<?> removeMenuById(Long menuId);

}
