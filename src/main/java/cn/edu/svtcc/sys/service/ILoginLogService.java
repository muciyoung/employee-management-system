package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.LoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface ILoginLogService extends IService<LoginLog> {

    List<LoginLog> getLogInfoByPage(LoginLog loginLog);

}
