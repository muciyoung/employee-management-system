package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.Docs;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 文件表 服务类
 * </p>
 *
 * @author muci
 * @since 2023-11-28
 */
public interface IDocsService extends IService<Docs> {

    /**
     * 根据ID删除单个记录。
     *
     * @param id 要删除的记录的ID
     * @return 删除操作的结果
     */
    Result<?> deleteById(Long id);

    /**
     * 批量删除记录。
     *
     * @param ids 要删除的记录的ID列表
     * @return 删除操作的结果
     */
    Result<?> deleteBatch(List<Long> ids);

    /**
     * 编辑现有记录。
     *
     * @param docs 包含更新信息的 Docs 对象
     * @return 编辑操作的结果
     */
    Result<?> edit(Docs docs);

    /**
     * 根据ID查找单个记录。
     *
     * @param id 要查找的记录的ID
     * @return 查找操作的结果
     */
    Result<?> findById(Long id);

    /**
     * 分页列出记录，可根据名称和上传者进行筛选。
     *
     * @param current 当前页数
     * @param size    每页记录数
     * @param oldName 要筛选的旧名称
     * @param uploader 要筛选的上传者
     * @return 列表操作的结果
     */
    Result<?> list(Integer current, Integer size, String oldName, String uploader);

    /**
     * 导出记录到 HttpServletResponse。
     *
     * @param response HttpServletResponse 对象，用于输出导出内容
     * @return 导出操作的结果
     */
    Result<?> export(HttpServletResponse response);

    /**
     * 上传文件并处理上传操作。
     *
     * @param file 要上传的文件
     * @return 上传操作的结果
     * @throws IOException 如果发生IO异常
     */
    Result<?> upload(MultipartFile file) throws IOException;

    /**
     * 下载指定文件名的记录。
     *
     * @param filename 要下载的文件名
     * @param response HttpServletResponse 对象，用于输出下载内容
     * @return 下载操作的结果
     * @throws IOException 如果发生IO异常
     */
    Result<?> download(String filename, HttpServletResponse response) throws IOException;


}
