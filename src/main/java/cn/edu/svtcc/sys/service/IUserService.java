package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.extension.service.IService;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface IUserService extends IService<User> {

    /**
     * 从redis中反序列化获取用户信息。
     *
     * @param token 用户的身份标识token
     * @return 包含用户信息的Map对象
     */
    Map<String, Object> getUserInfo(String token);

    /**
     * 注销登陆。
     *
     * @param token 用户的身份标识token
     * @return 注销是否成功的标志
     */
    Boolean logout(String token);

    /**
     * 添加用户，并更新用户角色对照表
     *
     * @param user 包含新增用户信息的 User 对象
     * @return 操作结果的标志
     */
    boolean addUser(User user);

    /**
     * 分页查询用户信息。
     *
     * @param pageNo   当前页数
     * @param pageSize 每页记录数
     * @param user     包含查询条件的 User 对象
     * @return 分页查询结果，包含用户及角色信息的分页查询结果
     */
    Result<Map<String, Object>> list(Long pageNo, Long pageSize, User user);

    /**
     * 更新用户信息，
     * 删除该用户原有用户角色对照表信息，
     * 重新写入用户角色对照表
     *
     * @param user 包含更新信息的 User 对象
     * @return 操作结果的标志
     */
    boolean updateUserById(User user);

    /**
     * 根据用户ID删除用户信息，
     * 同时删除关联的用户角色对照表信息
     * @param userId 用户ID
     * @return 操作结果的标志
     */
    boolean removeUserById(Long userId);

    /**
     * 根据用户名查询用户信息。
     * spring security所必须的接口
     *
     * @param username 用户名
     * @return 包含用户信息的 User 对象
     */
    User getUserByUserName(String username);

    /**
     * 根据用户ID获取用户所有的权限。
     *
     * @param userId 用户ID
     * @return 用户的权限信息
     */
    String getUserAuthorityById(Long userId);

    /**
     * 更新用户个人信息及头像。
     *
     * @param user  包含更新信息的 User 对象
     * @param request 从请求头中获取用户的身份标识token
     * @return 操作结果的标志
     */
    Result<?> updateUserAndAvatar(User user, HttpServletRequest request);
}
