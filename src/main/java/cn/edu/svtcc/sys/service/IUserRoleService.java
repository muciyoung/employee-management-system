package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-10-22
 */
public interface IUserRoleService extends IService<UserRole> {

}
