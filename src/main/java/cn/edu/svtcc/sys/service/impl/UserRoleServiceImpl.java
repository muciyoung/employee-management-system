package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.UserRole;
import cn.edu.svtcc.sys.mapper.UserRoleMapper;
import cn.edu.svtcc.sys.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-10-22
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
