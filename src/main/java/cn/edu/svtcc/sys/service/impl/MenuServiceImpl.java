package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.Menu;
import cn.edu.svtcc.sys.mapper.MenuMapper;
import cn.edu.svtcc.sys.service.IMenuService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;

    /**
     * 获取全部菜单
     *
     * @return {@link List}<{@link Menu}>
     */
    @Override
    public List<Menu> getAllMenu() {
        // 一级菜单
        LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<>();
        //查询出一级菜单
        wrapper.eq(Menu::getParentId,0);

        List<Menu> menuList = this.list(wrapper);
        // 填充子菜单
        setMenuChildren(menuList);
        return menuList;
    }

    @Override
    public List<Menu> getMenuListByUserId(Long userId) {
        //一级菜单
        List<Menu> menuList = menuMapper.getMenuByUserId(userId, 0L);
        setMenuChildrenByUserId(userId, menuList);
        return menuList;
    }

    @Override
    public Menu getMenuByTitle(String title) {
        LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<Menu>().eq(Menu::getTitle, title);
        Menu menu = menuMapper.selectOne(wrapper);
        if (!Objects.isNull(menu)){
            return menu;
        }
        return null;
    }

    @Override
    public boolean hasChildrenMenu(Long menuId) {
        LambdaQueryWrapper<Menu> wrapper = new LambdaQueryWrapper<Menu>().eq(Menu::getParentId, menuId);
        Long count = menuMapper.selectCount(wrapper);
        return count > 0;
    }

    @Override
    public Result<?> addMenu(Menu menu) {
        //判断菜单是否存在
        Menu menu1 =  this.getMenuByTitle(menu.getTitle());
        if (!Objects.isNull(menu1)){
            return Result.error(ResultCode.ERROR,"该菜单已被注册！");
        }
        if (this.menuMapper.insert(menu)>0){
            return Result.success("新增菜单成功！");
        }
        return Result.error(ResultCode.ERROR,"新增菜单失败！");
    }

    @Override
    public Result<?> updateMenu(Menu menu) {
        // 判断菜单是否存在
        Menu menu1 = this.menuMapper.selectById(menu.getMenuId());
        if (Objects.isNull(menu1)) {
            return Result.error(ResultCode.ERROR, "菜单不存在！");
        }
        // 检查title是否唯一
        Menu menuByTitle = this.getMenuByTitle(menu.getTitle());
        if (!Objects.isNull(menuByTitle) && !Objects.equals(menuByTitle.getMenuId(), menu.getMenuId())) {
            return Result.error(ResultCode.ERROR, "菜单名称重复！");
        }
        if(this.menuMapper.updateById(menu)>0){
            return Result.success("修改菜单成功！");
        }
        return Result.error(ResultCode.ERROR,"修改菜单失败！");
    }

    @Override
    public Result<?> removeMenuById(Long menuId) {
        // 判断菜单是否存在
        Menu menu = this.menuMapper.selectById(menuId);
        if (Objects.isNull(menu)) {
            return Result.error(ResultCode.ERROR, "菜单不存在！");
        }
        //判断是否有子菜单
        if (this.hasChildrenMenu(menuId)){
            return Result.error(ResultCode.ERROR,"该菜单存在子菜单，不可删除！");
        }
        //可以删除(逻辑删除)
        if(this.menuMapper.deleteById(menuId)>0){
            return Result.success("删除菜单成功！");
        }
        return Result.error(ResultCode.ERROR,"删除菜单失败！");
    }

    private void setMenuChildrenByUserId(Long userId, List<Menu> menuList) {
        if (menuList !=null){
            //遍历每一个一级菜单
            menuList.forEach(menu -> {
                //查处该用户所对应的子菜单
                List<Menu> subMenuList = menuMapper.getMenuByUserId(userId, menu.getMenuId());
                menu.setChildren(subMenuList);
                //递归
                setMenuChildrenByUserId(userId,subMenuList);
            });
        }
    }

    /**
     * 设置子菜单
     *
     * @param menuList 菜单列表
     */
    private void setMenuChildren(List<Menu> menuList) {
        if(menuList != null) {
            for (Menu menu:menuList) {
                LambdaQueryWrapper<Menu> subWrapper = new LambdaQueryWrapper<>();
                //eq 方法是 LambdaQueryWrapper 提供的方法，
                // 它表示 "等于" 条件。
                // 这个方法接受两个参数：第一个参数是实体类的字段 (在这里是 Menu::getParentId)，
                // 第二个参数是要匹配的值 (在这里是 menu.getMenuId())。

                //即这里的意思是：查询所有菜单的parentId等于一级菜单ID的所有记录
                //理解不了则换成常数：subWrapper.eq(Menu::getParentId, 1);
                // 可以看出这是要求查询出菜单的ParentId等于1的全部记录，对照数据库可以看出查出来的是二级路由菜单用户管理
                subWrapper.eq(Menu::getParentId, menu.getMenuId());

                //查询出一级菜单的所有子菜单
                List<Menu> subMenuList = this.list(subWrapper);
                //把子菜单list赋值给children
                menu.setChildren(subMenuList);
                // 递归
                setMenuChildren(subMenuList);
            }
        }
    }

}
