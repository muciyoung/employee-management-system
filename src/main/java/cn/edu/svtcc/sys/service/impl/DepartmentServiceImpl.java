package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.Department;
import cn.edu.svtcc.sys.mapper.DepartmentMapper;
import cn.edu.svtcc.sys.service.IDepartmentService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

    @Resource
    private DepartmentMapper departmentMapper;

    @Override
    public List<Department> getAllDepartment() {
        //查询一级部门
        LambdaQueryWrapper<Department> wrapper = new LambdaQueryWrapper<>();
        //这里默认一级部门为0
        wrapper.eq(Department::getParentId,0);
        List<Department> departmentList = departmentMapper.selectList(wrapper);
        //遍历一级部门，填充子部门
        setChildrenDepartment(departmentList);
        return departmentList;
    }

    @Override
    public boolean hasChildrenDepartment(Long departmentId) {
        //查询该部门是否存在子部门
        LambdaQueryWrapper<Department> wrapper = new LambdaQueryWrapper<Department>().eq(Department::getParentId, departmentId);
        return departmentMapper.selectCount(wrapper)>0;
    }

    @Override
    public Result<Map<String, Object>> list(Long pageNo, Long pageSize, Department department) {
        //构建分页对象
        Page<Department> page = new Page<>(pageNo, pageSize);

        //构建条件查询
        LambdaQueryWrapper<Department> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasLength(department.getDepartmentName()),Department::getDepartmentName,department.getDepartmentName());
        wrapper.orderByDesc(Department::getDepartmentId);
        Page<Department> departmentPage = this.departmentMapper.selectPage(page, wrapper);

        //构建返回参数
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows",departmentPage.getRecords());
        map.put("total",departmentPage.getTotal());
        return Result.success("查询成功！！",map);
    }

    @Override
    public Result<?> getDepartmentOptions() {
        LambdaQueryWrapper<Department> wrapper = new LambdaQueryWrapper<Department>().select(Department::getDepartmentId, Department::getDepartmentName);
        List<Department> departmentList = this.departmentMapper.selectList(wrapper);
        if (!departmentList.isEmpty()){
            return Result.success("部门列表获取成功！",departmentList);
        }
        return Result.error(ResultCode.ERROR,"部门列表获取失败！");
    }

    @Override
    public Result<?> findDepartmentById(Long departmentId) {
        Department department = this.departmentMapper.selectById(departmentId);
        if (!Objects.isNull(department)){
            return Result.success("获取单个部门信息成功！",department);
        }
        return Result.error(ResultCode.ERROR,"部门信息不存在！");
    }

    @Override
    public Result<?> addDepartment(Department department) {
        //参数校验
        if (Objects.isNull(department) || !StringUtils.hasLength(department.getDepartmentName())){
            return Result.error(ResultCode.ERROR,"部门信息不能为空 ！");
        }
        //查询是否存在该部门
        Department department1 = this.departmentMapper.selectOne(new LambdaQueryWrapper<Department>().eq(Department::getDepartmentName, department.getDepartmentName()));
        if (!Objects.isNull(department1)){
            return Result.error(ResultCode.ERROR,"该部门已存在！");
        }
        //上述校验都通过则进行添加, 先清除id，数据库自增
        department.setDepartmentId(null);
        if (this.departmentMapper.insert(department)>0){
            return Result.success("新增部门成功！");
        }
        return Result.error(ResultCode.ERROR,"新增部门成功失败");
    }

    @Override
    public Result<?> updateDepartment(Department department) {
        // 不可选择自己作为自己的上级部门
        if (Objects.equals(department.getDepartmentId(),department.getParentId())){
            return Result.error(ResultCode.ERROR,"当前部门不可选择！");
        }
        if (this.departmentMapper.updateById(department)>0){
            return Result.success("修改部门信息成功！");
        }
        return Result.error(ResultCode.ERROR,"修改部门信息失败");
    }

    @Override
    public Result<?> removeDepartmentById(Long departmentId) {
        //查询是否存在子部门，存在则无法删除
        if (this.hasChildrenDepartment(departmentId)){
            return Result.error(ResultCode.ERROR,"该部门存在子部门，无法删除！");
        }
        if (this.departmentMapper.deleteById(departmentId)>0){
            return Result.success("删除成功！");
        }
        return Result.error(ResultCode.ERROR,"删除失败！");
    }

    private void setChildrenDepartment(List<Department> departmentList) {
        if (departmentList!=null){
            departmentList.forEach(department -> {
                LambdaQueryWrapper<Department> wrapper = new LambdaQueryWrapper<>();
                //查询该部门所对应的所有子部门
                wrapper.eq(Department::getParentId,department.getDepartmentId());
                List<Department> subDepartmentList = departmentMapper.selectList(wrapper);
                //设置子部门
                department.setChildrenDept(subDepartmentList);
                //递归查询并设置子部门
                setChildrenDepartment(subDepartmentList);
            });
        }
    }


}
