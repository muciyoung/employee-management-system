package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.LoginLog;
import cn.edu.svtcc.sys.mapper.LoginLogMapper;
import cn.edu.svtcc.sys.service.ILoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog> implements ILoginLogService {
    @Autowired
    private LoginLogMapper loginLogMapper;
    @Override
    public List<LoginLog> getLogInfoByPage(LoginLog loginLog) {
        return loginLogMapper.getLogInfoByPage(loginLog);
    }
}
