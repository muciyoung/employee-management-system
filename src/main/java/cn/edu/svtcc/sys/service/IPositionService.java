package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.Position;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface IPositionService extends IService<Position> {

    /**
     * 根据职位ID查询职位信息。
     *
     * @param positionId 职位ID
     * @return 包含职位信息的查询结果
     */
    Result<?> findPositionById(Long positionId);

    /**
     * 获取所有职位的ID和name，用于职位选择框。
     *
     * @return 包含职位选择项的结果
     */
    Result<?> getPositionOptions();

    /**
     * 分页查询职位列表。
     *
     * @param pageNo    当前页数
     * @param pageSize  每页记录数
     * @param position  包含查询条件的 Position 对象
     * @return 分页查询结果，包括职位信息的列表和分页信息
     */
    Result<Map<String, Object>> list(Long pageNo, Long pageSize, Position position);

    /**
     * 新增职位。
     *
     * @param position 包含新增职位信息的 Position 对象
     * @return 操作结果，包括成功或失败的信息
     */
    Result<?> addPosition(Position position);

}
