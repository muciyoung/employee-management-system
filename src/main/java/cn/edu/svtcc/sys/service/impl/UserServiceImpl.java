package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.dto.UserRoleIdDto;
import cn.edu.svtcc.sys.entity.Menu;
import cn.edu.svtcc.sys.entity.Role;
import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.sys.entity.UserRole;
import cn.edu.svtcc.sys.mapper.RoleMenuMapper;
import cn.edu.svtcc.sys.mapper.UserMapper;
import cn.edu.svtcc.sys.mapper.UserRoleMapper;
import cn.edu.svtcc.sys.service.IMenuService;
import cn.edu.svtcc.sys.service.IUserService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.config.authentication.AuthenticationManagerFactoryBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private UserRoleMapper userRoleMapper;
    @Resource
    private RoleMenuMapper roleMenuMapper;
    @Resource
    private IMenuService menuService;

    @Override
    public Map<String, Object> getUserInfo(String token) {
        //根据token从redis反序列化获取用户信息
        Object object = redisTemplate.opsForValue().get(token);
        if (object!=null){
            //反序列化成对象
            User user = JSON.parseObject(JSON.toJSONString(object), User.class);
            HashMap<String, Object> data = new HashMap<>();
            //存入用户id方便给退出登陆写入日志表时传递userId
            data.put("userId",user.getUserId());
            //存入用户名
            data.put("name",user.getUsername());
            //存入头像地址
            data.put("avatar",user.getAvatarPath());
            //存入角色名称
            List<String> roleNameList = userMapper.getRoleNameByUserId(user.getUserId());
            data.put("roles",roleNameList);

            //获取菜单列表
            List<Menu> menuList = menuService.getMenuListByUserId(user.getUserId());
            data.put("menuList",menuList);
            return data;
        }
        return null;
    }

    @Override
    public Boolean logout(String token) {
        return redisTemplate.delete(token);
    }

    @Override
    @Transactional
    public boolean addUser(User user) {
        try {
            //插入用户表
            userMapper.insert(user);

            //插入用户角色对照表
            // 获取角色ID列表
            List<Long> roleIdList = user.getRoleIdList();
            if (!user.getRoleIdList().isEmpty()){
                //遍历角色ID
                roleIdList.forEach(roleId->{
                    userRoleMapper.insert(new UserRole(null,user.getUserId(),roleId));
                });
            }
            return true;
        }
        catch (Exception e){
            log.error("新增用户失败！",e);
            return false;
        }
    }

    @Override
    public Result<Map<String, Object>> list(Long pageNo, Long pageSize, User user) {
        //构建page分页查询对象
        Page<UserRoleIdDto> page = new Page<>(pageNo, pageSize);
        //调用自定义mapper方法返回page对象
        Page<UserRoleIdDto> dtoPage = this.userMapper.getAllUserWithRoleIds(page, user);
        //构建返回结果
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",dtoPage.getTotal());
        map.put("rows",dtoPage.getRecords());
        return Result.success("查询成功！",map);
    }

    @Override
    @Transactional
    public boolean updateUserById(User user) {
        try {
            //修改用户表
            userMapper.updateById(user);
            //未传入角色列表则不进行更新
            if (user.getRoleIdList()!=null){
                //删除用户角色对照表中的userId对应的数据
                LambdaQueryWrapper<UserRole> wrapper = new LambdaQueryWrapper<>();
                //注意这里不能对user对象进行判空，否则可能删除所有
                wrapper.eq(UserRole::getUserId,user.getUserId());
                userRoleMapper.delete(wrapper);

                //重新写入用户角色对照表
                List<Long> roleIdList = user.getRoleIdList();
                roleIdList.forEach(roleId->{
                    userRoleMapper.insert(new UserRole(null, user.getUserId(), roleId));
                });
            }
            return true;
        }
        catch (Exception e){
            log.error("更新用户信息失败",e);
            return false;
        }
    }

    @Override
    @Transactional
    public boolean removeUserById(Long userId) {
        try {
            //根据userId删除用户(这里由于配置了全局逻辑删除，所以并不会真正删除数据)
            userMapper.deleteById(userId);
            //根据userId从用户角色对照表(user_role)删除该userId的所有记录
            LambdaQueryWrapper<UserRole> wrapper = new LambdaQueryWrapper<>();
            //注意这里不能对user对象进行判空，否则可能删除所有
            wrapper.eq(UserRole::getUserId,userId);
            userRoleMapper.delete(wrapper);
            return true;
        }
        catch (Exception e){
            log.error("删除用户信息失败！",e);
            return false;
        }
    }

    @Override
    public User getUserByUserName(String username) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<User>().eq(User::getUsername, username);
        User user = userMapper.selectOne(wrapper);
        if (!Objects.isNull(user)){
            return user;
        }
        return null;
    }

    @Override
    public String getUserAuthorityById(Long userId) {
        StringBuilder authority = new StringBuilder();
        //根据用户id获取角色信息
        List<Role> roleList = userRoleMapper.getRolesByUserId(userId);
        if (!Objects.isNull(roleList)){
            String roleNames = roleList.stream().map(role -> "ROLE_" + role.getRoleName()).collect(Collectors.joining(","));
            authority.append(roleNames);

            //遍历角色获取菜单权限
            HashSet<String> hashSet = new HashSet<>();
            roleList.forEach(role -> {
                List<String> permissionList = roleMenuMapper.getPermissionByRoleId(role.getRoleId());
                hashSet.addAll(permissionList);
            });
            //拼接
            if (!hashSet.isEmpty()){
                authority.append(",");
                String permissions = String.join(",", hashSet);
                authority.append(permissions);
            }
            //打印测试
            System.out.println("authority = " + authority);
            return authority.toString();
        }
        return null;
    }


    private boolean updateUserAndAvatarPath(User user, String token) {
        //更新用户数据
        int count = userMapper.updateById(user);
        //更新redis
        Object object = redisTemplate.opsForValue().get(token);
        if (object!=null) {
            //反序列化成对象
            User redisUser = JSON.parseObject(JSON.toJSONString(object), User.class);
            //更新redis的数据
            redisUser.setUsername(user.getUsername());
            redisUser.setAvatarPath(user.getAvatarPath());
            //重新存储
            redisTemplate.opsForValue().set(token,redisUser,30, TimeUnit.MINUTES);
        }
        return count>0 && !Objects.isNull(object);
    }

    @Override
    public Result<?> updateUserAndAvatar(User user, HttpServletRequest request) {
        //校验数据
        User user1 = this.userMapper.selectById(user.getUserId());
        if (Objects.isNull(user1)){
            return Result.error(ResultCode.ERROR,"用户不存在 ！");
        }
        //用户名唯一性校验
        User user2 = this.getUserByUserName(user.getUsername());
        if (!Objects.isNull(user2) && !Objects.equals(user1.getUserId(),user2.getUserId())){
            return Result.error(ResultCode.ERROR,"该用户名已被使用 ！");
        }
        //更新用户数据和头像，同时更新redis
        String token = request.getHeader("X-Token");
        if (this.updateUserAndAvatarPath(user,token)){
            return Result.success("更新个人信息成功！");
        }
        return Result.error(ResultCode.ERROR,"更新个人信息失败！");
    }

}
