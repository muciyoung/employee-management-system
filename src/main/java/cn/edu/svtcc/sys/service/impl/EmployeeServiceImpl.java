package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.dto.EmployeeInfoDto;
import cn.edu.svtcc.sys.dto.EmployeeVo;
import cn.edu.svtcc.sys.entity.Employee;
import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.sys.mapper.EmployeeMapper;
import cn.edu.svtcc.sys.service.IEmployeeService;
import cn.edu.svtcc.sys.service.IUserService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import cn.edu.svtcc.utils.SystemConstants;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {
    @Resource
    private EmployeeMapper employeeMapper;
    @Resource
    private IUserService userService;
    @Resource
    private PasswordEncoder passwordEncoder;

    @Override
    public Page<EmployeeInfoDto> getEmployeeByPage(Long pageNo, Long pageSize, Employee employee) {
        return employeeMapper.getEmployeeDetailsByPage(new Page<>(pageNo, pageSize), employee);
    }

    @Override
    public Result<?> addEmployee(Employee employee) {
        //根据员工工号判断唯一性
        LambdaQueryWrapper<Employee> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Employee::getEmployeeCode,employee.getEmployeeCode());
        Employee employee1 = employeeMapper.selectOne(wrapper);
        // 已存在
        if (!Objects.isNull(employee1)){
            return Result.error(ResultCode.ERROR,"该员工已被注册！");
        }
        //可以注册
        if (employeeMapper.insert(employee)>0){
            return Result.success("新增员工成功！");
        }
        return Result.error(ResultCode.ERROR,"新增员工失败！");
    }

    @Override
    public Result<?> updateEmployee(Employee employee) {
        //不存在该员工
        Employee employee1 = employeeMapper.selectById(employee.getEmployeeId());
        if (Objects.isNull(employee1)){
            return Result.error(ResultCode.ERROR,"该员工不存在！");
        }

        Employee employee2 = this.employeeMapper.selectOne(new LambdaQueryWrapper<Employee>()
                .eq(Employee::getEmployeeCode, employee.getEmployeeCode())
                .ne(Employee::getEmployeeId, employee.getEmployeeId())
        );
        if (!Objects.isNull(employee2)){
            return Result.error(ResultCode.ERROR,"该工号已被注册！");
        }
        //存在则进行修改
        if (employeeMapper.updateById(employee)>0){
            return Result.success("更新员工成功！");
        }
        return Result.error(ResultCode.ERROR,"更新员工失败");
    }


    //TODO:屎山！！！！
    @Override
    public Result<?> assignEmployeeAccount(EmployeeVo employeeVo) {
        //拿出user相关信息
        User user = new User();
        user.setUsername(employeeVo.getUsername());
        user.setPassword(employeeVo.getPassword());
        user.setRoleIdList(new ArrayList<>());
        // 拿出员工ID
        Long employeeId = employeeVo.getEmployeeId();

        //参数校验： 判断用户名是否存在
        User user1 = userService.getUserByUserName(user.getUsername());
        if (!Objects.isNull(user1)){
            return Result.error(ResultCode.ERROR,"该账号已被注册 ！");
        }
        //密码加密，考虑可能用到spring security
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        //设置默认头像
        if (!StringUtils.hasLength(user.getAvatarPath())){
            user.setAvatarPath(SystemConstants.DEFAULT_AVATAR);
        }
        //将默认用户名密码插入用户表
       if (this.userService.addUser(user)){
           //通过username查出写入对象的userId
           User user2 = userService.getUserByUserName(user.getUsername());
           Long userId = user2.getUserId();

           //插入成功，将用户id写入员工表的user_Id
           Employee employee = new Employee();
           employee.setEmployeeId(employeeId);
           employee.setUserId(userId);
           if (this.employeeMapper.updateById(employee)>0){
               return Result.success("分配账号成功");
           }
       }
        return Result.error(ResultCode.ERROR,"分配账号失败 ！");
    }
}
