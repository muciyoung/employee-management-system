package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.RoleMenu;
import cn.edu.svtcc.sys.mapper.RoleMenuMapper;
import cn.edu.svtcc.sys.service.IRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author muci
 * @since 2023-10-21
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

}
