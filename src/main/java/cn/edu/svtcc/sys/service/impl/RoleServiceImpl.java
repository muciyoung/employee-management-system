package cn.edu.svtcc.sys.service.impl;

import cn.edu.svtcc.sys.entity.Role;
import cn.edu.svtcc.sys.entity.RoleMenu;
import cn.edu.svtcc.sys.mapper.RoleMapper;
import cn.edu.svtcc.sys.mapper.RoleMenuMapper;
import cn.edu.svtcc.sys.service.IMenuService;
import cn.edu.svtcc.sys.service.IRoleService;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import java.util.*;

/**
 * 角色服务实现类
 * 提供对角色及其关联菜单的增删改查功能
 *
 * @author muci
 * @since 2023-09-25
 */
@Service
@Slf4j
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private RoleMenuMapper roleMenuMapper;
    @Resource
    private IMenuService menuService;

    /**
     * 新增角色，并关联菜单权限
     *
     * @param role 要新增的角色对象
     */
    @Override
    @Transactional
    public void addRole(Role role) {
        //写入角色表
        roleMapper.insert(role);
        //写入角色菜单关联表
        setRoleMenu(role);
    }

    private void setRoleMenu(Role role) {
        if (role.getMenuIdList()!=null){
            role.getMenuIdList().forEach(menuId->{
                //给该角色写入所有选择的权限菜单
                roleMenuMapper.insert(new RoleMenu(null,menuId, role.getRoleId()));
            });
        }
    }

    /**
     * 根据角色ID查询角色信息，并获取关联菜单ID列表
     *
     * @param roleId 角色ID
     * @return 包含角色信息及关联菜单ID列表的角色对象
     */
    @Override
    public Role getRoleById(Long roleId) {
        //查询角色表
        Role role = roleMapper.selectById(roleId);
        if (role!=null){
            //查询菜单list
            List<Long> menuIdList = roleMenuMapper.getMenuIdListByRoleId(role.getRoleId());

            //TODO：这里只能返回没有子菜单的菜单id，否则传到前端会全选当前菜单下所有
            if (!Objects.isNull(menuIdList)){
                List<Long> resultList = new ArrayList<>();
                menuIdList.forEach(menuId->{
                    //只添加不存在子菜单的菜单
                    if (!menuService.hasChildrenMenu(menuId)){
                       resultList.add(menuId);
                    }
                });
                //填充菜单列表id
                role.setMenuIdList(resultList);
            }
            return role;
        }
        return null;
    }

    /**
     * 更新角色信息，包括删除原有关联菜单，并重新关联新的菜单权限
     *
     * @param role 包含更新信息的角色对象
     * @return 更新是否成功的标志
     */
    @Override
    @Transactional
    public boolean updateRoleById(Role role) {
        try {
            //修改角色表
            roleMapper.updateById(role);

            //删除原有角色ID的权限对照表
            LambdaQueryWrapper<RoleMenu> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(RoleMenu::getRoleId,role.getRoleId());
            roleMenuMapper.delete(wrapper);

            //重新新增角色权限
            setRoleMenu(role);
            return true;
        }catch (Exception e){
            log.error("更新角色信息失败！",e);
            return false;
        }
    }

    /**
     * 根据角色ID删除角色信息，包括删除关联的角色菜单关联表数据
     *
     * @param roleId 角色ID
     * @return 删除是否成功的标志
     */
    @Override
    @Transactional
    public boolean removeRoleById(Long roleId) {
        try {
            //删除角色表数据
            roleMapper.deleteById(roleId);

            //删除role_menu关联表数据
            LambdaQueryWrapper<RoleMenu> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(RoleMenu::getRoleId,roleId);
            roleMenuMapper.delete(wrapper);
            return true;
        }
        catch (Exception e){
            log.error("删除角色信息失败！",e);
            return false;
        }
    }

    /**
     * 分页查询角色列表
     *
     * @param pageNo    当前页数
     * @param pageSize  每页记录数
     * @param role      包含查询条件的角色对象
     * @return 分页查询结果，包括角色信息的列表和分页信息
     */
    @Override
    public Result<Map<String, Object>> list(Long pageNo, Long pageSize, Role role) {
        //构建分页器
        Page<Role> page = new Page<>(pageNo,pageSize);

        //构建查询条件
        LambdaQueryWrapper<Role> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.hasLength(role.getRoleName()),Role::getRoleName,role.getRoleName());
        wrapper.like(StringUtils.hasLength(role.getRoleDesc()),Role::getRoleDesc,role.getRoleDesc());

        //按照id降序排序
        wrapper.orderByDesc(Role::getRoleId);
        Page<Role> rolePage = this.roleMapper.selectPage(page, wrapper);
        //构建返回结果
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",rolePage.getTotal());
        map.put("rows",rolePage.getRecords());
        return Result.success("查询成功！",map);
    }


}
