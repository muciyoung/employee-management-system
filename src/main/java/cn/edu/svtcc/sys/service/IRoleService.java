package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.Role;
import cn.edu.svtcc.utils.Result;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface IRoleService extends IService<Role> {

    /**
     * 新增角色，并关联菜单权限。
     *
     * @param role 包含新增角色信息的 Role 对象
     */
    void addRole(Role role);

    /**
     * 根据角色ID查询角色信息，并获取关联菜单ID列表。
     *
     * @param roleId 角色ID
     * @return 包含角色信息及关联菜单ID列表的角色对象
     */
    Role getRoleById(Long roleId);

    /**
     * 更新角色信息，包括删除原有关联菜单，并重新关联新的菜单权限。
     *
     * @param role 包含更新信息的角色对象
     * @return 更新是否成功的标志
     */
    boolean updateRoleById(Role role);

    /**
     * 根据角色ID删除角色信息，包括删除关联的角色菜单关联表数据。
     *
     * @param roleId 角色ID
     * @return 删除是否成功的标志
     */
    boolean removeRoleById(Long roleId);

    /**
     * 分页查询角色列表。
     *
     * @param pageNo   当前页数
     * @param pageSize 每页记录数
     * @param role     包含查询条件的角色对象
     * @return 分页查询结果，包括角色信息的列表和分页信息
     */
    Result<Map<String, Object>> list(Long pageNo, Long pageSize, Role role);

}
