package cn.edu.svtcc.sys.service;

import cn.edu.svtcc.sys.entity.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author muci
 * @since 2023-10-21
 */
public interface IRoleMenuService extends IService<RoleMenu> {

}
