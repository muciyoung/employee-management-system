package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.entity.Role;
import cn.edu.svtcc.sys.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-10-22
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    List<Role> getRolesByUserId(@Param("userId") Long userId);

}
