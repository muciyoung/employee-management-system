package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.entity.Position;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface PositionMapper extends BaseMapper<Position> {

}
