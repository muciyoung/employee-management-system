package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.entity.Docs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 文件表 Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-11-28
 */
public interface DocsMapper extends BaseMapper<Docs> {

    /**
     * 关联查询user表和docs
     * @param page
     * @param oldName
     * @param uploader
     * @return page
     */
    IPage<Docs> selectFileByPage(IPage<Docs> page, @Param("oldName") String oldName, @Param("uploader") String uploader);
}
