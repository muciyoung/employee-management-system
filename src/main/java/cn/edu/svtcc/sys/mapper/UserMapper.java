package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.dto.UserRoleIdDto;
import cn.edu.svtcc.sys.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface UserMapper extends BaseMapper<User> {

    List<String> getRoleNameByUserId(Long userId);

    /**
     * 查询所有用户
     * 关联每个用户的角色
     * @return
     */
    Page<UserRoleIdDto> getAllUserWithRoleIds(Page<UserRoleIdDto> page, @Param("user") User user);

}
