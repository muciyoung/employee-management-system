package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.dto.EmployeeInfoDto;
import cn.edu.svtcc.sys.entity.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

    Page<EmployeeInfoDto> getEmployeeDetailsByPage(Page<EmployeeInfoDto> page, @Param("employee") Employee employee);
}
