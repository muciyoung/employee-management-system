package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.entity.Menu;
import cn.edu.svtcc.sys.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-10-21
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    /**
     * 权限菜单回显
     * 查询该角色的所有菜单ID(二级菜单)，
     * 因为查一级菜单在页面渲染后会全选
     * @param roleId
     * @return
     */
    List<Long> getMenuIdListByRoleId(Long roleId);


    List<String> getPermissionByRoleId(@Param("roleId") Long roleId);

}
