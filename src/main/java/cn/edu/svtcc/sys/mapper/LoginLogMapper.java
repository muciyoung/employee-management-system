package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.entity.LoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface LoginLogMapper extends BaseMapper<LoginLog> {
    List<LoginLog> getLogInfoByPage(LoginLog loginLog);

}
