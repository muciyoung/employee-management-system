package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface MenuMapper extends BaseMapper<Menu> {
    public List<Menu> getMenuByUserId(Long userId,Long parentId);


}
