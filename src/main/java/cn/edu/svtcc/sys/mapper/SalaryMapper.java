package cn.edu.svtcc.sys.mapper;

import cn.edu.svtcc.sys.entity.Salary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
public interface SalaryMapper extends BaseMapper<Salary> {

}
