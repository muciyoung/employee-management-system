package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 角色表(Role)实体类
 *
 * @author makejava
 * @since 2023-10-20 14:36:08
 */
@Data
public class Role implements Serializable {
    private static final long serialVersionUID = 814744515506704988L;
    /**
     * 角色id
     */
    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色描述
     */
    private String roleDesc;

    /**
     * 用于接收前端带过来的菜单节点数组menuIdList
     */
    @TableField(exist = false)
    private List<Long> menuIdList;

}

