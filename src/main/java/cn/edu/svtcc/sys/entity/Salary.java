package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <p>
 * 
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Salary implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "salary_id", type = IdType.AUTO)
    private Long salaryId;

    /**
     * 薪资金额
     */
    private BigDecimal salary;

    /**
     * 薪资等级 (P1, P2, P3...)
     */
    private String salaryLevel;

    /**
     * 生效日期
     */
    private LocalDate effectiveDate;

    /**
     * 员工ID
     */
    private Long employeeId;
}
