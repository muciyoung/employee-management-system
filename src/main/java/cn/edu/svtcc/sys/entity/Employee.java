package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "employee_id", type = IdType.AUTO)
    private Long employeeId;

    /**
     * 员工姓名
     */
    private String fullName;

    /**
     * 员工工号，例如：GH2023-0001
     */
    private String employeeCode;

    /**
     * 性别 (1=男, 2=女)
     */
    private String sex;

    /**
     * 电话号码
     */
    private String phoneNumber;

    /**
     * 入职日期
     */
    private LocalDate hireDate;

    /**
     * 所属部门ID
     */
    private Long departmentId;

    /**
     * 职位ID
     */
    private Long positionId;

    /**
     * 用户ID
     */
    private Long userId;

}
