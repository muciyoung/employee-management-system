package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;

/**
 * (Department)实体类
 *
 * @author makejava
 * @since 2023-11-07 19:51:42
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department implements Serializable {
    private static final long serialVersionUID = -76818233836153878L;

    @TableId(type = IdType.AUTO)
    private Long departmentId;

    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 部门电话
     */
    private String departmentPhone;

    /**
     * 部门地址
     */
    private String address;

    /**
     * 所属上级部门编号
     */
    private Long parentId;

    /**
     * 所属上级部门名称
     */
    private String parentName;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 逻辑删除
     * 1:已删除
     * 0:未删除
     */
    @TableLogic
    private String deleted;


    @TableField(exist = false)
    //children为空则json中不包含children属性
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Department> childrenDept;
}

