package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 日志用户名字段
     */
    @TableField(exist = false)
    private String userName;

    /**
     * 登录时间
     */
    private LocalDateTime loginTime;

    /**
     * IP地址
     */
    private String ipAddress;

    /**
     * 客户端类型
     */
    private String clientType;

    /**
     * 浏览器及版本
     */
    private String browserVersion;

    /**
     * 操作系统
     */
    private String operatingSystem;

    //自定义构造
    public LoginLog(Long logId, String logType, Long userId, LocalDateTime loginTime, String ipAddress, String clientType, String browserVersion, String operatingSystem) {
        this.logId = logId;
        this.logType = logType;
        this.userId = userId;
        this.loginTime = loginTime;
        this.ipAddress = ipAddress;
        this.clientType = clientType;
        this.browserVersion = browserVersion;
        this.operatingSystem = operatingSystem;
    }
}
