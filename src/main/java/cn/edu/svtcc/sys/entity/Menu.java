package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单表(Menu)实体类
 *
 */
@Data
public class Menu implements Serializable {

    private static final long serialVersionUID = 214333403171215906L;
    /**
     * 菜单ID，自增主键
     */
    @TableId(value = "menu_id",type = IdType.AUTO)
    private Long menuId;

    /**
     * 组件名称
     */
    private String component;

    /**
     * 菜单路径
     */
    private String path;

    /**
     * 重定向路径
     */
    private String redirect;

    /**
     * 菜单(路由)名称
     */
    private String name;

    /**
     * 菜单(路由)标题
     */
    private String title;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 父菜单ID
     */
    private Long parentId;

    /**
     * 父菜单名称
     */
    private String parentName;

    /**
     * 权限编码
     */
    private String permissionCode;

    /**
     * 菜单类型：0-目录，1-菜单，2-按钮
     */
    private String menuType;

    /**
     * 是否为叶子菜单（Y/N）
     */
    private String isLeaf;

    /**
     * 是否隐藏（1 表示隐藏，0 表示不隐藏）
     */
    private Integer hidden;

    /**
     * 逻辑删除
     * 1:已删除
     * 0:未删除
     */
    @TableLogic
    private String deleted;

    /**
     * 映射子菜单(路由)对象
     */
    @TableField(exist = false)
    //JsonInclude.Include.NON_EMPTY 表示只有当字段的值不为空（不为 null，且不为空字符串）时，
    // 才会被包含在序列化后的 JSON 数据中。如果字段的值为空，那么在生成的 JSON 中将不包含该字段。
    //这里主要是因为生成的路由节点中一级路由不包含children
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Menu> children;

    /**
     * 映射路由的meta对象
     * meta: { title: '角色管理', icon: 'tree' }
     */
    @TableField(exist = false)
    private Map<String,Object> meta = new HashMap<>();

    //构造meta属性的值
    public Map<String,Object> getMeta(){
        meta.put("title",this.title);
        meta.put("icon",this.icon);
        return this.meta;
    }


}

