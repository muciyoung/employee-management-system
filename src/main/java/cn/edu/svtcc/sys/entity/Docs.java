package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 文件表
 * </p>
 *
 * @author muci
 * @since 2023-11-28
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Docs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "file_id", type = IdType.AUTO)
    private Long fileId;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件类型
     */
    private String type;

    /**
     * 文件的原名称
     */
    private String oldName;

    /**
     * 文件md5信息
     */
    private String md5;

    /**
     * 文件大小KB
     */
    private Long size;

    /**
     * 文件上传者id
     */
    private Long createdId;

    /**
     * 上传人
     */
    @TableField(exist = false)
    //@JsonInclude()
    private String uploader;

    /**
     * 文件备注
     */
    private String remark;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 0未删除，1已删除，默认为0
     */
    private Boolean deleted;
}
