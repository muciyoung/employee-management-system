package cn.edu.svtcc.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@TableName("user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"isEnabled","isAccountNonExpired", "isAccountNonLocked", "isCredentialsNonExpired", "authorities"})
public class User implements Serializable, UserDetails {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 用户名
     */
    @TableField(value = "user_name")
    private String username;

    /**
     * 性别：1男2女
     */
    private String sex;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;
    /**
     * 状态，1启用，0停用
     */
    private String status;

    /**
     * 逻辑删除
     * 1:已删除
     * 0:未删除
     */
    @TableLogic
    private String deleted;

    /**
     * 头像文件路径
     */
    private String avatarPath;
    /**
     * 用户角色列表
     */
    @TableField(exist = false)
    private List<Long> roleIdList;

    //spring security相关字段
    /**
     * 账号是否过期
     * 1-未过期
     * 0-过期
     */
    private boolean isAccountNonExpired = true;

    /**
     * 账号是否锁定
     * 1-未锁定
     * 0-锁定
     */
    private boolean isAccountNonLocked = true;

    /**
     * 密码是否过期
     * 1-未过期
     * 0-过期
     */
    private boolean isCredentialsNonExpired = true;

    /**
     * 账户是否可用
     * 1-可用
     * 0-不可用
     */
    private boolean isEnabled = true;

    /**
     * 权限列表
     */
    @TableField(exist = false)
    Collection<? extends GrantedAuthority> authorities;

    public User(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }
}
