package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.Salary;
import cn.edu.svtcc.sys.service.ISalaryService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Api(tags = {"薪资等级接口"})
@RestController
@RequestMapping("/salary")
public class SalaryController {
    @Autowired
    private ISalaryService salaryService;


    @ApiOperation("查询所有薪资等级")
    @GetMapping
    public Result<?> getSalaryLevel(){
        List<Salary> list = salaryService.list();
        return Result.success("success",list);
    }

    /**
     * 按页面获取工资
     *
     * @param pageNo   页码
     * @param pageSize 页面大小
     * @param salary   工资
     * @return {@link Result}<{@link ?}>
     */
    @ApiOperation(value = "薪资等级分页查询接口")
    @GetMapping("/page")
    public Result<?> getSalariesByPage(@RequestParam(value = "pageNo",defaultValue = "1")Long pageNo,
                                       @RequestParam(value = "pageSize",defaultValue = "5")Long pageSize,
                                       Salary salary){
        //构建分页器
        Page<Salary> page = new Page<>(pageNo,pageSize);
        //构建查询条件
        LambdaQueryWrapper<Salary> wrapper = new LambdaQueryWrapper<>();
        //按照薪资等级查询
        wrapper.eq(StringUtils.hasLength(salary.getSalaryLevel()),Salary::getSalaryLevel,salary.getSalaryLevel());
        Page<Salary> userPage = salaryService.page(page, wrapper);
        //构建返回结果
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",userPage.getTotal());
        map.put("rows",userPage.getRecords());
        return Result.success("查询成功！",map);

    }


    /**
     * 添加工资水平
     *
     * @param salary 工资
     * @return {@link Result}<{@link ?}>
     */
    @ApiOperation(value = "添加薪资等级")
    @PostMapping("/add")
    public Result<?> addSalaryLevel(@RequestBody Salary salary){
        salary.setEffectiveDate(LocalDate.now());
        if (!StringUtils.hasLength(salary.getSalaryLevel())){
            return Result.error(ResultCode.ERROR,"传入的数据不能为null!");
        }
        boolean saved = salaryService.save(salary);
        if (!saved){
            return Result.error(ResultCode.ERROR,"新增失败!");
        }
        return Result.success("添加成功!",null);
    }

}
