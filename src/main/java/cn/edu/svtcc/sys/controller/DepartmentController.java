package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.Department;
import cn.edu.svtcc.sys.service.IDepartmentService;
import cn.edu.svtcc.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Api(tags = {"部门相关接口"})
@RestController
@RequestMapping("/department")
public class DepartmentController {
    @Resource
    private IDepartmentService departmentService;

    @ApiOperation("根据id获取部门信息")
    @GetMapping("/{id}")
    public Result<?> getDepartmentById(@PathVariable("id") Long departmentId){
        return this.departmentService.findDepartmentById(departmentId);
    }

    @ApiOperation("动态获取部门的options")
    @GetMapping("/options")
    public Result<?> getDepartmentNameOptions(){
        return this.departmentService.getDepartmentOptions();
    }

    @ApiOperation("部门分页条件查询接口")
    @GetMapping("/page")
    public Result<Map<String,Object>> getAllDepartmentInfoByPage(@RequestParam(defaultValue = "1") Long pageNo,
                                                                 @RequestParam(defaultValue = "5") Long pageSize,
                                                                 Department department){
        return this.departmentService.list(pageNo,pageSize,department);
    }

    @ApiOperation("新增部门接口")
    @PostMapping
    public Result<?> addDepartment(@RequestBody Department department){
        return this.departmentService.addDepartment(department);
    }

    @ApiOperation("更新部门信息接口")
    @PutMapping
    public Result<?> updateDepartment(@RequestBody Department department){
        return this.departmentService.updateDepartment(department);
    }

    @ApiOperation("根据id删除部门")
    @DeleteMapping("/{id}")
    public Result<?> deleteDepartmentById(@PathVariable("id") Long departmentId){
        return this.departmentService.removeDepartmentById(departmentId);
    }

    @GetMapping
    @ApiOperation("查询所有部门数据")
    public Result<?> getDepartmentList(){
        List<Department> departmentList = departmentService.getAllDepartment();
        return Result.success("部门列表获取成功！",departmentList);
    }

    @GetMapping("/treeOptions")
    @ApiOperation("部门树形选择框")
    public Result<?> getDepartmentTreeOptions(){
        List<Department> departmentList = departmentService.getAllDepartment();
        //构建顶层树的选项
        Department department = new Department();
        department.setDepartmentId(0L);
        department.setDepartmentName("顶层部门");
        departmentList.add(department);
        return Result.success("部门列表获取成功！",departmentList);
    }
}
