package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.LoginLog;
import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.sys.service.ILoginLogService;
import cn.edu.svtcc.sys.service.IUserService;
import cn.edu.svtcc.utils.LogWriteUtil;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import cn.edu.svtcc.utils.SystemConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Slf4j
@RestController
@RequestMapping("/user")
@Api(tags = {"用户接口列表"})
public class UserController {
    @Resource
    private IUserService userService;
    @Resource
    private ILoginLogService logService;
    @Resource
    private PasswordEncoder passwordEncoder;

    @GetMapping("/{id}")
    @ApiOperation("根据userId查询用户")
    public Result<User> getUserInfoById(@PathVariable("id") Long userId){
        User user = userService.getById(userId);
        if (!Objects.isNull(user)){
            return Result.success(user);
        }
        return Result.error(ResultCode.ERROR,"查询失败！");
    }

    @GetMapping("/username/{name}")
    @ApiOperation("根据userName查询单个用户")
    public Result<?> getUserInfoByUserName(@PathVariable("name") String userName){
        User user = userService.getUserByUserName(userName);
        if (!Objects.isNull(user)){
            return Result.success(user);
        }
        return Result.error(ResultCode.ERROR);
    }


    // 根据token从redis获取登陆信息，反序列化拿到用户信息
    @GetMapping("/info")
    @ApiOperation("读取用户信息接口")
    public Result<Map<String,Object>> getUserInfo(@RequestParam("token") String token){
        Map<String,Object> data = userService.getUserInfo(token);
        if (!Objects.isNull(data)){
            return Result.success("查询成功！",data);
        }
        //返回前端拦截器指定的50014则会立马重置前端的token，退出登陆
        return Result.error(ResultCode.LOGIN_EXPIRED,"用户信息已过期，请重登陆！");
    }


    // 关联每个用户对应角色，分页查询
    @GetMapping("/page")
    @ApiOperation("用户信息分页条件查询")
    @PreAuthorize("hasAnyAuthority('sys:user:list')")
    public Result<Map<String,Object>> getUserInfoByPage(@RequestParam(defaultValue = "1") Long pageNo,
                                                        @RequestParam(defaultValue = "5") Long pageSize,
                                                        User user){
        return this.userService.list(pageNo,pageSize,user);
    }


    //TODO：引入了spring security，这个接口不一定会要
    @PostMapping("/logout")
    @ApiOperation("退出登陆接口")
    public Result<?> logout(@RequestHeader("X-Token") String token,@Autowired HttpServletRequest request){
        //写入退出日志
        Map<String, Object> userInfo = userService.getUserInfo(token);
        if (userInfo!=null){
            //这里写入日志表时需要用户id
            Long userId = (Long) userInfo.get("userId");
            LoginLog logoutLog;
            try {
                logoutLog = LogWriteUtil.logWrite(request, userId,"退出登陆");
            }catch (Exception e){
                log.warn("------设备退出异常！-------"+request.getHeader("User-Agent"));
                return Result.error(ResultCode.ERROR,"设备退出异常！！");
            }
            logService.save(logoutLog);
            Boolean isLogout = userService.logout(token);
            if (isLogout){
                return Result.success(ResultCode.LOGIN_EXPIRED,"注销成功",null);
            }
        }
        return Result.error(ResultCode.ERROR,"注销失败！");
    }


    // 添加用户，并将角色写入用户角色对照表
    @PostMapping
    @ApiOperation("添加用户")
    @PreAuthorize("hasAnyAuthority('sys:user:add')")
    public Result<?> addUser(@RequestBody User user){
        //判断用户名是否存在
        User user1 = userService.getUserByUserName(user.getUsername());
        if (!Objects.isNull(user1)){
            return Result.error(ResultCode.ERROR,"该用户名已被注册！");
        }
        //密码加密，考虑可能用到spring security
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        //设置默认头像
       if (!StringUtils.hasLength(user.getAvatarPath())){
           user.setAvatarPath(SystemConstants.DEFAULT_AVATAR);
       }
        if (userService.addUser(user)){
            return Result.success("新增成功！");
        }
        return Result.error(ResultCode.ERROR,"新增失败！");
    }


    // 更新用户，并更新用户角色对照表
    @PutMapping
    @ApiOperation("根据userId更新用户")
    @PreAuthorize("hasAnyAuthority('sys:user:edit')")
    public Result<?> updateUser(@RequestBody User user){
        //判断用户是否存在
        if (userService.getById(user.getUserId())==null){
            return Result.error(ResultCode.ERROR,"用户不存在！");
        }
        //验证用户名唯一性
        User user1 = userService.getUserByUserName(user.getUsername());
        if (!Objects.isNull(user1) && !Objects.equals(user1.getUserId(),user.getUserId())){
            return Result.error(ResultCode.ERROR,"该用户名已被使用！");
        }
        //暂不修改密码
        user.setPassword(null);
        if (userService.updateUserById(user)){
            return Result.success("更新成功！");
        }
        return Result.error(ResultCode.ERROR,"更新失败！");
    }


    // 按ID删除用户，同时删除对应的用户角色对照表对应数据
    @ApiOperation("根据userId删除单个用户")
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('sys:user:del')")
    public Result<?> deleteUserById(@PathVariable("id") Long userId){
        if (userService.removeUserById(userId)){
            return Result.success("删除成功！");
        }
        return Result.error(ResultCode.ERROR,"删除失败！");
    }


    // 更新头像及个人数据，同时更新redis
    @PutMapping("/avatar")
    @ApiOperation("更新redis的头像及个人信息")
    public Result<?> updateUserAndAvatarPath(@RequestBody User user, HttpServletRequest request){
        return this.userService.updateUserAndAvatar(user,request);
    }

}
