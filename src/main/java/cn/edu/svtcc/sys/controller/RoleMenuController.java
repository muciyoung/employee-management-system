package cn.edu.svtcc.sys.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-10-21
 */
@Controller
@RequestMapping("/sys/roleMenu")
public class RoleMenuController {

}
