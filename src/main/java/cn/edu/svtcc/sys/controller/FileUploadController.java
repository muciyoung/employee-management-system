package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.service.IDocsService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/file")
public class FileUploadController {

    @Value("${upload.path}")
    private String uploadPath;

    @Resource
    private IDocsService docsService;

    @PostMapping("/upload")
    public Result<?> handleFileUpload(@RequestPart("img") MultipartFile file, HttpServletResponse response) throws IOException {
        // 设置响应头，禁用浏览器缓存
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");

        //参数校验
        if (file.isEmpty()) {
            return Result.error(ResultCode.ERROR,"头像上传失败！");
        }
        //格式校验
        if (!"image/jpeg".equals(file.getContentType()) && !"image/png".equals(file.getContentType())){
            System.out.println("文件格式错误");
            return Result.error(ResultCode.ERROR,"上传头像图片只能是 JPG 或 PNG 格式！");
        }
        try {
            // 生成规范的文件名
            String originalFileName = file.getOriginalFilename();
            String extension = StringUtils.getFilenameExtension(originalFileName);
            String generatedFileName = UUID.randomUUID()+ "." + extension;

            // 构造上传文件的保存路径
            String filePath = uploadPath + File.separator + generatedFileName;
            File uploadedFile = new File(filePath);

            // 将文件保存到服务器指定位置
            file.transferTo(uploadedFile);

            // 构建相对于应用程序根路径的文件访问路径
            String relativeFilePath = "/uploads" + File.separator + generatedFileName;
            Map<String, String> data = new HashMap<>();
            data.put("avatarPath", relativeFilePath);
            return Result.success("头像上传成功！",data);

        } catch (IOException e) {
            log.error("头像上传异常!",e);
            return Result.error(ResultCode.ERROR,"头像上传异常！");
        }
    }
}
