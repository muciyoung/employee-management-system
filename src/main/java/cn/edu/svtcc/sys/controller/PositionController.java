package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.Position;
import cn.edu.svtcc.sys.service.IPositionService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Api(tags = {"职位相关接口"})
@RestController
@RequestMapping("/position")
public class PositionController {
    @Resource
    private IPositionService positionService;

    @GetMapping("/{id}")
    @ApiOperation("根据Id查询职位信息")
    public Result<?> getPositionById(@PathVariable("id") Long positionId){
        return this.positionService.findPositionById(positionId);
    }

    @GetMapping("/options")
    @ApiOperation("动态获取职位的的options")
    public Result<?> getPositionNameOptions(){
        return this.positionService.getPositionOptions();
    }


    @GetMapping("/page")
    @ApiOperation("职位分页条件查询接口")
    public Result<Map<String,Object>> getAllPositionInfoByPage(@RequestParam(defaultValue = "1") Long pageNo,
                                                               @RequestParam(defaultValue = "5") Long pageSize,
                                                               Position position){
        return this.positionService.list(pageNo,pageSize,position);
    }

    @PostMapping
    @ApiOperation("新增职位接口")
    public Result<?> addPosition(@RequestBody Position position){
        return this.positionService.addPosition(position);
    }

    @PutMapping
    @ApiOperation("修改职位信息")
    public Result<?> updatePosition(@RequestBody Position position){
        if (positionService.updateById(position)){
            return Result.success("更新职位成功！");
        }
        return Result.error(ResultCode.ERROR,"更新职位失败！！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除职位信息")
    public Result<?> deletePosition(@PathVariable("id") Long positionId){
        if (positionService.removeById(positionId)){
            return Result.success("删除职位成功！");
        }
        return Result.error(ResultCode.ERROR,"删除职位失败！！");
    }

}
