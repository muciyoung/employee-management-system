package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.dto.EmployeeInfoDto;
import cn.edu.svtcc.sys.dto.EmployeeVo;
import cn.edu.svtcc.sys.entity.Employee;
import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.sys.service.IEmployeeService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Api(tags = {"员工相关接口列表"})
@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Resource
    private IEmployeeService employeeService;

    @ApiOperation("根据id查询员工信息")
    @GetMapping("/{id}")
    public Result<?> getEmployeeById(@PathVariable("id") Long employeeId){
        Employee employee = employeeService.getById(employeeId);
        if (!Objects.isNull(employee)){
            return Result.success("查询员工信息成功！",employee);
        }
        return Result.error(ResultCode.ERROR,"查询员工信息失败！");
    }

    /**
     * 按页面获取所有员工信息
     *
     * @param pageNo   页码
     * @param pageSize 页面大小
     * @param employee 员工
     * @return {@link Result}<{@link Map}<{@link String}, {@link Object}>>
     */
    @ApiOperation("员工信息多表分页关联查询接口")
    @GetMapping("/page")
    public Result<Map<String, Object>> getAllEmployeeInfoByPage(@RequestParam(defaultValue = "1") Long pageNo,
                                                                @RequestParam(defaultValue = "5") Long pageSize,
                                                                Employee employee) {
        Page<EmployeeInfoDto> employeePage = employeeService.getEmployeeByPage(pageNo, pageSize, employee);
        //封装返回结果
        HashMap<String, Object> map = new HashMap<>();
        map.put("total", employeePage.getTotal());
        map.put("rows", employeePage.getRecords());
        return Result.success( "查询成功！", map);
    }

    /**
     * 添加员工
     * @param employee
     * @return Result
     */
    @ApiOperation("新增员工接口")
    @PostMapping
    public Result<?> addEmployee(@RequestBody Employee employee){
        return this.employeeService.addEmployee(employee);
    }


    /**
     * 更新员工
     *
     */
    @ApiOperation("修改员工信息接口")
    @PutMapping
    public Result<?> updateEmployee(@RequestBody Employee employee){
        return this.employeeService.updateEmployee(employee);
    }


    /**
     * 删除员工
     * @param employeeId
     * @return Result
     */
    @ApiOperation("删除员工接口")
    @DeleteMapping("/{id}")
    public Result<?> deleteEmployee(@PathVariable("id") Long employeeId){
        if (employeeService.removeById(employeeId)){
            return  Result.success("删除员工成功！");
        }
        return Result.error(ResultCode.ERROR,"删除员工失败！");
    }

    @PostMapping("/assignAccount")
    @ApiOperation("员工分配账号")
    public Result<?> assignAccount(@RequestBody EmployeeVo employeeVo){
        return this.employeeService.assignEmployeeAccount(employeeVo);
    }

}
