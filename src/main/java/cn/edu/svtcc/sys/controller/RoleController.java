package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.Role;
import cn.edu.svtcc.sys.service.IRoleService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Api(tags = {"角色接口列表"})
@Slf4j
@RestController
@RequestMapping("/role")
public class RoleController {
    @Resource
    private IRoleService roleService;

    @GetMapping("all")
    public Result<List<Role>> getAllRole(){
        return Result.success("获取所有角色信息成功！",this.roleService.list());
    }


    // 按ID获取角色信息，并获取角色关联的菜单ID列表
    @GetMapping("/{id}")
    @ApiOperation("根据roleId查询角色")
    public Result<?> getRoleById(@PathVariable("id") Long roleId){
        Role role = roleService.getRoleById(roleId);
        if (!Objects.isNull(role)){
            return Result.success("查询角色成功！",role);
        }
        return Result.error(ResultCode.ERROR,"查询角色失败！");
    }


    @GetMapping("/page")
    @ApiOperation("角色信息分页条件查询接口")
    public Result<Map<String,Object>> getRoleInfoByPage(@RequestParam(defaultValue = "1") Long pageNo,
                                                        @RequestParam(defaultValue = "5") Long pageSize,
                                                        Role role){
        return this.roleService.list(pageNo,pageSize,role);
    }


    // 新增角色，并写入角色菜单关联表，关联菜单权限
    @PostMapping
    @ApiOperation("添加角色(同时写入该角色的菜单权限)")
    public Result<?> addRole(@RequestBody Role role){
        //写入角色表，同时写入该角色的权限菜单
        roleService.addRole(role);
        return Result.success("新增角色成功！");
    }


    // 根据角色ID删除角色信息，包括删除关联的角色菜单关联表数据
    @DeleteMapping("/{id}")
    @ApiOperation("根据roleId删除单个角色")
    public Result<?> deleteRoleById(@PathVariable("id") Long roleId){
        if (roleService.removeRoleById(roleId)){
            return Result.success("删除角色成功！");
        }
        return Result.error(ResultCode.ERROR,"删除失败！");
    }


    // 更新角色信息，包括删除原有关联菜单，并重新关联新的菜单权限
    @PutMapping
    @ApiOperation("更新角色")
    public Result<?> updateRole(@RequestBody Role role){
        //根据角色id去查询是否存在该条记录
        Role selectRole = roleService.getById(role.getRoleId());
        //记录不存在
        if (Objects.isNull(selectRole)){
            return Result.error(ResultCode.ERROR,"更新失败,该记录不存在！");
        }
        //记录存在，通过修改后的角色名称进行查询，如果修改后的角色名称存在，且id不等于当前传入的roleId则说明角色名称已存在，则不可以修改，否则就可以修改
        //或者可以理解为：查询是否有除了自己以外还有人使用这个角色名称的人
        Role serviceOne = roleService.getOne(
                new LambdaQueryWrapper<Role>()
                        .eq(Role::getRoleName, role.getRoleName())
                        //ne不等于
                        .ne(Role::getRoleId,role.getRoleId())
        );
        //想要修改的角色名已存在(已被使用)
        if (!Objects.isNull(serviceOne)){
            return Result.error(ResultCode.ERROR,"更新失败,该角色名称或描述已存在！");
        }
        //允许修改(除自己外未被使用)
       if (roleService.updateRoleById(role)){
           return Result.success("更新成功！");
       }
       return Result.error(ResultCode.ERROR,"更新失败！");
    }

}
