package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.LoginLog;
import cn.edu.svtcc.sys.service.ILoginLogService;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-09-25
 */
@Api(tags = {"日志接口列表"})
@RestController
@RequestMapping("/loginLog")
public class LoginLogController {

    @Autowired
    private ILoginLogService logService;


    /**
     * 按页面获取日志
     *
     * @param pageNum  页码
     * @param pageSize 页面大小
     * @param loginLog 登录日志
     * @return {@link Result}<{@link List}<{@link LoginLog}>>
     */
    @ApiOperation(value = "日志分页条件查询接口")
    @GetMapping("/page")
    public Result<Map<String,Object>> getLogByPage(@RequestParam(value = "pageNum",defaultValue = "1")Long pageNum,
                                                   @RequestParam(value = "pageSize",defaultValue = "5")Long pageSize,
                                                   LoginLog loginLog){
        //构建查询条件
        LambdaQueryWrapper<LoginLog> wrapper = new LambdaQueryWrapper<>();
        //根据用户id进行查询(这里实际应该根据用户名进行查询)
//        wrapper.eq(StringUtils.hasLength(loginLog.getUserId().toString()),LoginLog::getUserId,loginLog.getUserId());
        //根据ip地址
        wrapper.like(StringUtils.hasLength(loginLog.getIpAddress()),LoginLog::getIpAddress,loginLog.getIpAddress());
        //根据客户端类型
        wrapper.like(StringUtils.hasLength(loginLog.getClientType()),LoginLog::getClientType,loginLog.getClientType());
        //根据浏览器
        wrapper.like(StringUtils.hasLength(loginLog.getBrowserVersion()),LoginLog::getBrowserVersion,loginLog.getBrowserVersion());
        IPage<LoginLog> page = logService.page(new Page<>(pageNum,pageSize),wrapper);

        HashMap<String, Object> map = new HashMap<>();

        map.put("rows",page.getRecords());
        map.put("total",page.getTotal());
        map.put("size",page.getSize());
        return Result.success("查询成功！",map);
    }


    /**
     * 获取所有日志信息
     *
     * @param loginLog 登录日志
     * @return {@link Result}<{@link List}<{@link LoginLog}>>
     */
    @ApiOperation(value = "查询所有日志")
    @GetMapping
    public Result<List<LoginLog>> getAllLogInfo(LoginLog loginLog){
        return Result.success(logService.getLogInfoByPage(loginLog));
    }


    /**
     * 批量删除日志
     *
     * @param logIds 日志标识
     * @return {@link Result}<{@link ?}>
     */
    @ApiOperation(value = "批量删除日志")
    @DeleteMapping("/delete/batch/{logIds}")
    public Result<?> batchDeleteLogs(@PathVariable String logIds){
        List<Long> logIdList = Arrays.stream(logIds.split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        boolean isDelete = logService.removeBatchByIds(logIdList);
        if (isDelete){
            return Result.success("批量删除成功！",null);
        }
        return Result.error(ResultCode.ERROR,"批量删除失败！");
    }

    /**
     * 按日志 ID 删除日志
     *
     * @param logId 日志标识
     * @return {@link Result}<{@link ?}>
     */
    @ApiOperation(value = "删除单个日志")
    @DeleteMapping("/delete/{logId}")
    public Result<?> deleteLogByLogId(@PathVariable Long logId){
        boolean isDelete = logService.removeById(logId);
        if (isDelete){
            return Result.success("删除成功！",null);
        }
        return Result.error(ResultCode.ERROR,"删除失败！");
    }
}
