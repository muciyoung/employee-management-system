package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.Docs;
import cn.edu.svtcc.sys.service.IDocsService;
import cn.edu.svtcc.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 文件表 前端控制器
 * </p>
 *
 * @author muci
 * @since 2023-11-28
 */
@RestController
@RequestMapping("/docs")
@Api(tags = {"文件管理相关接口列表"})
public class DocsController {
    @Resource
    private IDocsService docsService;

    @GetMapping("/{id}")
    @ApiOperation("查询")
    public Result<?> findById(@PathVariable Long id) {
        return this.docsService.findById(id);
    }

    @GetMapping
    @ApiOperation("分页条件查询")
    public Result<?> list(@RequestParam(defaultValue = "1") Integer pageNo,
                          @RequestParam(defaultValue = "5") Integer pageSize,
                          String oldName,String uploader) {
        return this.docsService.list(pageNo, pageSize, oldName,uploader);
    }

    @PutMapping
    @ApiOperation("编辑更新")
    public Result<?> edit(@RequestBody Docs docs) {
        return this.docsService.edit(docs);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("逻辑删除")
    public Result<?> deleteById(@PathVariable Long id) {
        return this.docsService.deleteById(id);
    }

    @DeleteMapping("/batch/{ids}")
    @ApiOperation("批量逻辑删除")
    public Result<?> deleteBatch(@PathVariable List<Long> ids) {
        return this.docsService.deleteBatch(ids);
    }

    @PostMapping("/upload")
    @ApiOperation("文件上传")
    public Result<?> upload(MultipartFile file) throws IOException {
        return this.docsService.upload(file);
    }

    @GetMapping("/download/{filename}")
    @ApiOperation("文件下载")
    public Result<?> download(@PathVariable("filename") String filename, HttpServletResponse response) throws IOException {
        return this.docsService.download(filename, response);
    }

    //TODO:未完成
    @GetMapping("/export")
    @ApiOperation("数据导出接口")
    public Result<?> export(HttpServletResponse response) throws IOException {
        return this.docsService.export(response);
    }

}
