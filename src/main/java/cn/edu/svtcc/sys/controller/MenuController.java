package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.Menu;
import cn.edu.svtcc.sys.service.IMenuService;
import cn.edu.svtcc.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;


@Slf4j
@Api(tags = {"菜单相关接口列表"})
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Resource
    private IMenuService menuService;

    @GetMapping
    @ApiOperation("查询所有菜单数据")
    public Result<?> getAllMenu(){
        List<Menu> menuList =  menuService.getAllMenu();
        return Result.success("菜单列表获取成功！",menuList);
    }

    @PostMapping
    @ApiOperation("新增菜单")
    public Result<?> addMenu(@RequestBody Menu menu){
        return this.menuService.addMenu(menu);
    }

    @PutMapping
    @ApiOperation("修改菜单")
    public Result<?> updateMenu(@RequestBody Menu menu){
        return this.menuService.updateMenu(menu);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除菜单")
    public Result<?> deleteMenu(@PathVariable("id") Long menuId){
        return this.menuService.removeMenuById(menuId);
    }

    @GetMapping("/treeOptions")
    @ApiOperation("菜单树形选择框")
    public Result<?> getMenuTreeOptions(){
        List<Menu> menuList = menuService.getAllMenu();
        //构建顶层菜单树的选项
        Menu menu = new Menu();
        menu.setMenuId(0L);
        menu.setTitle("顶级菜单");
        menuList.add(menu);
        return Result.success("菜单列表获取成功！",menuList);
    }

}
