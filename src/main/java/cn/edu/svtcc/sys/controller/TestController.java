package cn.edu.svtcc.sys.controller;

import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.sys.mapper.UserMapper;
import cn.edu.svtcc.utils.ExcelExportUtil;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * 获取Flask服务接口数据测试
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/09/27
 */
@Api(tags = {"Flask服务接口数据测试"})
@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private UserMapper userMapper;

    /**
     * 获取Flask服务的天气信息
     *
     * @return {@link Result}<{@link ?}>
     */
    @ApiOperation(value = "获取Flask服务天气数据接口")
    @GetMapping("/weather/{city}")
    public Result<?> getFlaskWeatherData(@PathVariable String city) {
        // Flask API的URL
        String flaskApiUrl = "http://localhost:8088/weather/" + city; // 请替换为实际的 Flask API 地址

        // 创建 RestTemplate
        RestTemplate restTemplate = new RestTemplate();

        try {
            // 发送 GET 请求到 Flask API
            ResponseEntity<HashMap> response = restTemplate.getForEntity(flaskApiUrl, HashMap.class);

            // 检查响应状态码，通常应该是 HttpStatus.OK (200)
            if (response.getStatusCode().is2xxSuccessful()) {
                log.info("flask服务端获取天气数据成功！\n" + response.getBody());
                return Result.success("flask服务端获取天气数据成功！", response.getBody());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return Result.error(ResultCode.ERROR, "flask服务端获取天气数据异常！！");
    }

    /**
     * 导出数据
     */
    @GetMapping("/export/user")
    @PreAuthorize("hasAnyAuthority('sys:user:export')")
    public Result<?> exportUserExcel(HttpServletResponse response) throws IOException {
        // 根据条件在数据库查询数据
        List<User> userList = userMapper.selectList(null);
        //调用工具类导出数据
        ExcelExportUtil.exportToExcel(response, userList, "用户信息", User.class, "用户信息表");
        return Result.success("数据导出成功！");
    }
}
