package cn.edu.svtcc.sys.controller;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * 验证码生成控制器
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/10/16
 */
@Api(tags = {"验证码接口"})
@Controller
@RequestMapping("/captcha")
public class CaptchaController {
    @Resource
    private Producer captchaProducer;
    @Resource
    private Producer captchaProducerMath;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    /**
     * 验证码生成
     */
    @ApiOperation("字符/算数验证码生成接口")
    @GetMapping(value = "/captchaImage")
    public ModelAndView getKaptchaImage(HttpServletRequest request, HttpServletResponse response)
    {
        ServletOutputStream out = null;
        try
        {
            HttpSession session = request.getSession();
            response.setDateHeader("Expires", 0);
            //禁止图像缓存
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            response.setContentType("image/jpeg");

            String type = request.getParameter("type");
            String capStr;
            String code = null;
            BufferedImage bi = null;
            if ("math".equals(type))//验证码为算数 8*9 类型
            {
                String capText = captchaProducerMath.createText();
                capStr = capText.substring(0, capText.lastIndexOf("@"));
                code = capText.substring(capText.lastIndexOf("@") + 1);
                bi = captchaProducerMath.createImage(capStr);
            }
            else if ("char".equals(type))//验证码为 abcd类型
            {
                capStr = code = captchaProducer.createText();
                bi = captchaProducer.createImage(capStr);
            }
            //通过redis设置验证码过期时效
            if (code != null) {
                //暂用sessionId，跨域的话
                redisTemplate.opsForValue().set("captch:"+session.getId(),code,30,TimeUnit.SECONDS);
            }
            session.setAttribute(Constants.KAPTCHA_SESSION_KEY, code);
            // 将图像输出到Servlet输出流中
            out = response.getOutputStream();
            System.out.println("out = " + out);
            if (bi != null) {
                ImageIO.write(bi, "jpg", out);
            }
            System.out.println("1存入session里面的code："+session.getAttribute(Constants.KAPTCHA_SESSION_KEY));
            System.out.println("1--session.getId() = " + session.getId());
            out.flush();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (out != null)
                {
                    out.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }
}
