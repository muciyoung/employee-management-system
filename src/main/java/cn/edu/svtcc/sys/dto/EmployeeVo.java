package cn.edu.svtcc.sys.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分配账号VO
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeVo {
    private String username;
    private String password;
    private List<Long> roleIdList;
    private Long employeeId;
}
