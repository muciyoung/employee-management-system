package cn.edu.svtcc.sys.dto;

import lombok.Data;
import java.time.LocalDate;


/**
 * 员工信息数据传输对象（DTO）
 *
 * 该类用于封装员工相关信息，包括员工ID、全名、性别、电话号码、入职日期，
 * 以及所属部门和职位的相关信息。
 */
@Data
public class EmployeeInfoDto {
    /**
     * 员工ID
     */
    private Long employeeId;

    /**
     * 员工全名
     */
    private String fullName;

    /**
     * 员工工号，例如：GH2023-0001
     */
    private String employeeCode;

    /**
     * 员工性别
     */
    private String sex;

    /**
     * 员工电话号码
     */
    private String phoneNumber;

    /**
     * 员工入职日期
     */
    private LocalDate hireDate;

    /**
     * 所属部门的ID
     */
    private Long departmentId;

    /**
     * 所属部门的名称
     */
    private String departmentName;

    /**
     * 员工的职位ID
     */
    private Long positionId;

    /**
     * 员工的职位名称
     */
    private String positionName;

    /**
     * 关联的用户ID
     */
    private Long userId;

    /**
     * 关联的用户名
     */
    private String username;
}

