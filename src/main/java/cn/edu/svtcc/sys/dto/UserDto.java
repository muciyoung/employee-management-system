package cn.edu.svtcc.sys.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

import java.util.Date;

@Data
public class UserDto {

    //设置excel表头名称
    @ExcelProperty("用户编号")
    @ColumnWidth(20)
    private Long id;

    /**
     * 设置该列的名称为”用户名“；
     * */
    @ExcelProperty("用户名")
    /**
     * 设置表格列的宽度为20；
     * */
    @ColumnWidth(20)
    private String username;

    /**
     * 导出时忽略该字段
     * */
    @ExcelIgnore
    private String password;

    @ExcelProperty("昵称")
    @ColumnWidth(20)
    private String nickname;

    @ExcelProperty("生日")
    @ColumnWidth(20)
    /**
      按照指定的格式对日期进行格式化；
      */
    @DateTimeFormat("yyyy-MM-dd")
    private Date birthday;

    @ExcelProperty("手机号")
    @ColumnWidth(20)
    private String phone;

    @ExcelProperty("身高（米）")
    @NumberFormat("#.##")
    @ColumnWidth(20)
    private Double height;


}
