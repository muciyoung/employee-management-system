package cn.edu.svtcc.sys.dto;

import lombok.Data;
import org.springframework.util.StringUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class UserRoleIdDto implements Serializable {

    private Long userId; // 用户ID，唯一标识用户

    private String username; // 用户名，用于登录和身份标识

    private String email; // 用户的电子邮件地址

    private String status; // 用户状态

    private String avatarPath; // 用户头像文件路径

    private List<Long> roleIdList; // 用户所属角色ID列表，表示用户的角色和权限


    /**
     * 自定义setter处理逻辑
     * @param roleIdList
     */
    public void setRoleIdList(String roleIdList) {
        if (!StringUtils.hasLength(roleIdList)){
            this.roleIdList  = null;
        }
        List<Long> roles = new ArrayList<>();
        List<String> stringList= Arrays.asList(roleIdList.split(","));
        stringList.forEach(s -> {
            roles.add(Long.valueOf(s));
        });
        this.roleIdList = roles;
    }
}
