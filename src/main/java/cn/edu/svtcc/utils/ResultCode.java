package cn.edu.svtcc.utils;

/**
 * 结果代码
 */
public class ResultCode {

    /**
     * 成功的状态码
     */
    public static final Integer SUCCESS = 20000;
    /**
     * 失败的状态码
     */
    public static final Integer ERROR = 500;
    /**
     * 未登录状态码(匿名用户)
     */
    public static final Integer NO_LOGIN = 401;
    /**
     * 无权限状态码
     */
    public static final Integer NO_AUTH = 403;
    /**
     * 登陆过期
     */
    public static final Integer LOGIN_EXPIRED = 50014;

    /**
     * 验证码过期
     */
    public static final Integer VERIFICATION_CODE_EXPIRED = 40102;



}
