package cn.edu.svtcc.utils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 返回工具类
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/20
 */
public class WebUtils {
    public static void renderString(HttpServletResponse response, String message){
        try
        {
            response.setStatus(200);
            response.setContentType("application/json");
            response.setCharacterEncoding ("utf-8") ;
            response.getWriter().print(message);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
