package cn.edu.svtcc.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 *Excel Export util
 * </p>
 *
 * Excel数据导出工具类
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/10/24
 */
public class ExcelExportUtil {

    private static void setExcelResponseProp(HttpServletResponse response, String rawFileName) throws UnsupportedEncodingException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(rawFileName.concat(".xlsx"), "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName);
    }

    public static <T> void exportToExcel(HttpServletResponse response, List<T> dataList, String sheetName, Class<T> dataClass, String fileName) throws IOException {
        OutputStream outputStream = response.getOutputStream();
        try {
            setExcelResponseProp(response, fileName);
            EasyExcel.write(outputStream, dataClass)
                    .excelType(ExcelTypeEnum.XLSX)
                    .sheet(sheetName)
                    .doWrite(dataList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            outputStream.flush();
            outputStream.close();
        }
    }
}
