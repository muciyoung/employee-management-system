package cn.edu.svtcc.utils;

import com.alibaba.fastjson2.JSON;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

/**
 * JWT工具类
 * 创建和解析访问令牌
 */
@Component
public class JwtUtil {
    private static final long JWT_EXPIRE = 30*60*1000L; //半小时
    private static final String JWT_KEY = "123456";

    /**
     * 创建令牌
     *
     * @param data 数据
     * @return {@link String}
     */
    public String createToken (Object data){
        //当前时间
        long currentTime = System.currentTimeMillis();
        //过期时间
        long expTime = currentTime + JWT_EXPIRE;
        //构建JWT
        JwtBuilder builder = Jwts.builder()
                .setId(UUID.randomUUID() + "")
                .setSubject(JSON.toJSONString(data))
                .setIssuer("system")
                .setIssuedAt(new Date(currentTime))
                .signWith(SignatureAlgorithm.HS256, encodeSecret(JWT_KEY))
                .setExpiration(new Date(expTime));
        return builder.compact();
    }

    /**
     * 编码密钥
     *
     * @param key 钥匙
     * @return {@link SecretKey}
     */
    private SecretKey encodeSecret(String key) {
        byte[] encode = Base64.getEncoder().encode(key.getBytes());
        return new SecretKeySpec(encode,0,encode.length,"AES");
    }

    /**
     * 解析令牌
     *
     * @param token 令 牌
     * @return {@link Claims}
     */
    public Claims parseToken(String token){
        Claims body = Jwts.parser()
                .setSigningKey(encodeSecret(JWT_KEY))
                .parseClaimsJws(token)
                .getBody();
        return body;
    }

    /**
     * 解析令牌
     *
     * @param token 令 牌
     * @param clazz 类名
     * @return {@link T}
     */
    public <T> T parseToken(String token, Class<T> clazz){
        Claims body = Jwts.parser()
                .setSigningKey(encodeSecret(JWT_KEY))
                .parseClaimsJws(token)
                .getBody();
        return JSON.parseObject(body.getSubject(),clazz);
    }

}