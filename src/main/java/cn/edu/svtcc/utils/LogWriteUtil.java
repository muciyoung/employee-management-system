package cn.edu.svtcc.utils;

import cn.edu.svtcc.sys.entity.LoginLog;
import eu.bitwalker.useragentutils.UserAgent;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Locale;

/**
 * 日志写入工具类
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/09/27
 */
public class LogWriteUtil {
    /**
     * 日志写入
     *
     * @param request 请求
     * @param userId  用户标识
     * @return {@link LoginLog}
     */
    public static LoginLog logWrite(HttpServletRequest request,Long userId,String logType) {
        //获取ip
        String userIp = request.getRemoteAddr();
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("user-agent"));
        //操作系统
        String operatingSystem = userAgent.getOperatingSystem().getName();
        //浏览器以及版本
        String browserVersion = userAgent.getBrowser().toString().toLowerCase(Locale.ROOT)+"_"+userAgent.getBrowserVersion().getVersion();
        //客户端类型：电脑or手机
        String clientType = userAgent.getOperatingSystem().getDeviceType().toString().toLowerCase(Locale.ROOT);

        //创建日志对象封装数据
        return new LoginLog( null, logType,userId, LocalDateTime.now(),userIp,clientType,browserVersion,operatingSystem);
    }
}
