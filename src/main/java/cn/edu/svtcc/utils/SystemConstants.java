package cn.edu.svtcc.utils;

/**
 * 系统常量
 *
 * @author muci
 * @date 2023/11/10
 */
public class SystemConstants {
    /**
     * 系统默认头像
     */
    public static final String DEFAULT_AVATAR = "https://img1.baidu.com/it/u=1285375996,3783960243&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500";
}
