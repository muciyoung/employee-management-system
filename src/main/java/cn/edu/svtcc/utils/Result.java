package cn.edu.svtcc.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 统一返回结果
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/09/27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    /**
     * 返回码
     */
    private Integer code;
    /**
     * 返回消息
     */
    private String message;
    /**
     * 返回数据
     */
    private T data;

    public static <T> Result<T> ok() {
        return new Result<>(ResultCode.SUCCESS, "操作成功！", null);
    }

    // 静态方法1：返回成功响应，无数据
    public static <T> Result<T> success() {
        return new Result<>(ResultCode.SUCCESS, "操作成功！", null);
    }

    // 静态方法2：返回成功响应，包含数据
    public static <T> Result<T> success(T data) {
        return new Result<>(ResultCode.SUCCESS, "操作成功！", data);
    }

    // 返回成功响应，无数据
    public static <T> Result<T> success(String message) {
        return new Result<>(ResultCode.SUCCESS, message, null);
    }
    // 静态方法：返回自定义成功消息，包含数据
    public static <T> Result<T> success(String message, T data) {
        return new Result<>(ResultCode.SUCCESS, message, data);
    }

    // 静态方法：返回自定义成功响应，包含数据
    public static <T> Result<T> success(Integer code, String message, T data) {
        return new Result<>(code, message, data);
    }
    

    // 静态方法：返回错误响应，无数据
    public static <T> Result<T> error(Integer code, String message) {
        return new Result<>(code, message, null);
    }

    // 静态方法：返回错误响应，包含数据
    public static <T> Result<T> error(Integer code, String message, T data) {
        return new Result<>(code, message, data);
    }

    // 静态方法：返回错误响应码
    public static <T> Result<T> error(Integer code) {
        return new Result<>(code, "操作失败！", null);
    }
}


