package cn.edu.svtcc.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * 全局跨域处理
 */
@Configuration
public class MyCorsConfig {
    @Bean
    public CorsFilter corsFilter(){
        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.addAllowedOrigin("http://10.70.135.105:9528");
        configuration.addAllowedOriginPattern("*");
//        configuration.addAllowedOrigin("http://localhost:9528");
        //允许的请求头信息
        configuration.addAllowedHeader("*");
        //允许的方法
        configuration.addAllowedMethod("*");
        //允许跨域请求时是否发送身份验证信息（如Cookie和HTTP认证）的配置。
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
        corsConfigurationSource.registerCorsConfiguration("/**",configuration);

        return new CorsFilter(corsConfigurationSource);
    }
}
