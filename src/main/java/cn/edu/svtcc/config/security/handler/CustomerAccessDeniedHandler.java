package cn.edu.svtcc.config.security.handler;

import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import cn.edu.svtcc.utils.WebUtils;
import com.alibaba.fastjson2.JSON;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 认证用户访问无权限资源时处理器
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/16
 */
@Component
public class CustomerAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        //构建返回数据
        Result<T> resultData = Result.error(ResultCode.NO_AUTH,"无权限访问，请联系管理员！");
        //调用工具类进行返回
        WebUtils.renderString(response, JSON.toJSONString(resultData));
    }
}
