package cn.edu.svtcc.config.security;

import cn.edu.svtcc.config.security.filter.ValidateCaptchaFilter;
import cn.edu.svtcc.config.security.handler.AnonymousAuthenticationHandler;
import cn.edu.svtcc.config.security.handler.CustomerAccessDeniedHandler;
import cn.edu.svtcc.config.security.handler.LoginFailureHandler;
import cn.edu.svtcc.config.security.handler.LoginSuccessHandler;
import cn.edu.svtcc.config.security.filter.ValidateTokenFilter;
import cn.edu.svtcc.exception.UserCountLockException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import javax.annotation.Resource;

/**
 * Security配置类
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/16
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SpringSecurityConfig {
    private static final String[] URL_WHITE_LIST = {
            "/login",
            "/uploads/**",
            "/favicon.ico",
            "/swagger-resources/**", "/webjars/**",
            "/v2/**", "/swagger-ui.html/**", "/api",
            "/api-docs", "/api-docs/**", "/doc.html/**",
            "/user/login","/user/info","/user/logout","/error",
            "/captcha/**"
    };
    @Resource
    private LoginSuccessHandler loginSuccessHandler;
    @Resource
    private LoginFailureHandler loginFailureHandler;
    @Resource
    private AnonymousAuthenticationHandler authenticationHandler;
    @Resource
    private CustomerAccessDeniedHandler accessDeniedHandler;
    @Resource
    private ValidateTokenFilter validateTokenFilter;
    @Resource
    private ValidateCaptchaFilter validateCaptchaFilter;
    @Resource
    private CustomerUserDetailsService userDetailsService;

    //BCryptPasswordEncoder加密
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .formLogin()//表单登陆
                .successHandler(loginSuccessHandler)//认证成功处理器
                .failureHandler(loginFailureHandler)//认证失败处理器
                .and()
                // 基于 token，不需要 csrf
                .csrf().disable()
                // 基于 token，不需要 session
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and() //不创建session
                // 设置 jwtAuthError 处理认证失败、鉴权失败
                .exceptionHandling()
                .authenticationEntryPoint(authenticationHandler) //匿名无权限访问
                .accessDeniedHandler(accessDeniedHandler) //认证用户无权限访问
                .and()
                .cors() //支持跨域
                .and()
                // 下面开始设置权限
                .authorizeRequests(authorize -> authorize
                        // 登陆匿名访问
                        .antMatchers(URL_WHITE_LIST).permitAll()
                        // 请求放开
                        // 其他地址的访问均需验证权限
                        .anyRequest().authenticated()
                )
                // 添加 JWT 过滤器，JWT 过滤器在用户名密码认证过滤器之前

                //验证码过滤器
                .addFilterBefore(validateCaptchaFilter, UsernamePasswordAuthenticationFilter.class)

                // token过滤器
                .addFilterBefore(validateTokenFilter, UsernamePasswordAuthenticationFilter.class)
                // 认证用户时用户信息加载配置，注入springAuthUserService
                .userDetailsService(userDetailsService)
                .build();
    }
}