package cn.edu.svtcc.config.security.handler;

import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.WebUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 發录认证成功处理器类
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/16
 */
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // 获取登陆的用户信息
        User user = (User) authentication.getPrincipal();
        //暂用UUID,终极方案jwt
        String token = "user:"+ UUID.randomUUID();
        //存入redis
        redisTemplate.opsForValue().set(token,user,30, TimeUnit.MINUTES);

        HashMap<String, Object> data = new HashMap<>();
        data.put("token",token);
        String jsonData = JSON.toJSONString(Result.success("登陆成功！", data), SerializerFeature.DisableCircularReferenceDetect);
        WebUtils.renderString(response,jsonData);
    }
}
