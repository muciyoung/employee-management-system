package cn.edu.svtcc.config.security.filter;

import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import cn.edu.svtcc.utils.WebUtils;
import com.alibaba.fastjson.JSON;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 验证验证码过滤器
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/22
 */
@Component
public class ValidateCaptchaFilter extends OncePerRequestFilter {

    private static final String DEFAULT_FILTER_PROCESS_URL = "/login";
    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //拦截登陆接口，校验验证码
        if (request.getRequestURI().equals(DEFAULT_FILTER_PROCESS_URL)){
            // 获取验证码
            String captcha = request.getParameter("captcha");
            //验证码非空校验
            if (!StringUtils.hasLength(captcha)){
                String resultJson = JSON.toJSONString(Result.error(ResultCode.ERROR, "验证码不能为空！"));
                WebUtils.renderString(response,resultJson);
                return ;
            }
            //验证码过期
            HttpSession session = request.getSession();
            String redisCode = (String) redisTemplate.opsForValue().get("captch:"+session.getId());
            if (!StringUtils.hasLength(redisCode)){
                String resultJson = JSON.toJSONString(Result.error(ResultCode.VERIFICATION_CODE_EXPIRED, "验证码已过期！"));
                WebUtils.renderString(response,resultJson);
                return ;
            }
            //校验验证码是否正确
            if (!redisCode.equals(captcha)){
                String resultJson = JSON.toJSONString(Result.error(ResultCode.ERROR, "验证码错误！"));
                WebUtils.renderString(response,resultJson);
                return ;
            }
        }
        filterChain.doFilter(request,response);
    }
}
