package cn.edu.svtcc.config.security.handler;

import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import cn.edu.svtcc.utils.WebUtils;
import com.alibaba.fastjson2.JSON;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 匿名用户访问无权限资源时处理器
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/16
 */
@Component
public class AnonymousAuthenticationHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        //构建返回数据
        Result<T> resultData = Result.error(ResultCode.NO_LOGIN,"匿名用户，无权限访问 ！");
        //调用工具类进行返回
        WebUtils.renderString(response, JSON.toJSONString(resultData));
    }
}
