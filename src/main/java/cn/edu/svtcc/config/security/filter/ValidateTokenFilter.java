package cn.edu.svtcc.config.security.filter;

import cn.edu.svtcc.config.security.CustomerUserDetailsService;
import cn.edu.svtcc.config.security.handler.LoginFailureHandler;
import cn.edu.svtcc.sys.entity.User;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * token校验过滤器
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/20
 */
@Slf4j
@Component
public class ValidateTokenFilter extends OncePerRequestFilter {

    @Resource
    private CustomerUserDetailsService customerUserDetailsService;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private LoginFailureHandler loginFailureHandler;
    private static final String[] URL_WHITE_LIST = {
            "/login",
            "/favicon.ico",
            "/swagger-resources/**", "/webjars/**",
            "/v2/**", "/swagger-ui.html/**", "/api",
            "/api-docs", "/api-docs/**", "/doc.html/**",
            "/user/login","/user/info","/user/logout","/error",
            "/captcha/**"
    };



    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String token = request.getHeader("X-Token");
            System.out.println("请求url:"+request.getRequestURI());
            // 如果token是空 或者 url在白名单里，则放行
            if(!StringUtils.hasLength(token) || new ArrayList<String>(Arrays.asList(URL_WHITE_LIST)).contains(request.getRequestURI()) || request.getRequestURI().startsWith("/uploads")){
                filterChain.doFilter(request,response);
                return;
            }
            //否则开始校验
            this.validateToken(request);
        }catch (AuthenticationException e){
            loginFailureHandler.onAuthenticationFailure(request,response,e);
        }
        //登陆请求不需要验证，直接放行
        filterChain.doFilter(request,response);
    }

    private void validateToken(HttpServletRequest request) {
        String token = request.getHeader("X-Token");
        //校验token
        Object object = redisTemplate.opsForValue().get(token);
        //redis的token不存在
        if (Objects.isNull(object)){
            throw new RuntimeException("token不存在或已过期！");
        }
        //存在，解析成对象
        User user = JSON.parseObject(JSON.toJSONString(object), User.class);
        String username = user.getUsername();
        //判断用户名非空
        if (!StringUtils.hasLength(username)){
            throw new RuntimeException("token解析失败！");
        }
        UserDetails userDetails = customerUserDetailsService.loadUserByUsername(username);
        if (Objects.isNull(userDetails)){
            throw new RuntimeException("token解析失败！");
        }
        //创建用户身份认证对象
        UsernamePasswordAuthenticationToken  usernamePasswordAuthenticationToken= new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
        //设置请求信息
        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        //将认证的信息交给spring security上下文章】
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }
}
