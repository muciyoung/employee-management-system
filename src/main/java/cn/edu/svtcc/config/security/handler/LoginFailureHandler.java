package cn.edu.svtcc.config.security.handler;

import cn.edu.svtcc.exception.UserCountLockException;
import cn.edu.svtcc.utils.Result;
import cn.edu.svtcc.utils.ResultCode;
import cn.edu.svtcc.utils.WebUtils;
import com.alibaba.fastjson2.JSON;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 用戶认证失败处理类
 *
 * @author maibangbangtangsongdemacbookpro
 * @date 2023/11/16
 */
@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        // 这里是针对Security的不同类型的不同返回状态码 以及对应的 状态返回信息
        Result<T> resultData;
        if (exception instanceof BadCredentialsException) {
            resultData = Result.error(ResultCode.ERROR, "用户名或密码错误，登录失败！");
        } else if (exception instanceof LockedException) {
            resultData = Result.error(ResultCode.ERROR, "账户被锁定，登录失败！");
        } else if (exception instanceof CredentialsExpiredException) {
            resultData = Result.error(ResultCode.ERROR, "密码过期，登录失败！");
        } else if (exception instanceof AccountExpiredException) {
            resultData = Result.error(ResultCode.ERROR, "账户过期，登录失败！");
        } else if (exception instanceof DisabledException) {
            resultData = Result.error(ResultCode.ERROR, "账户被禁用，登录失败！");
        } else if (exception instanceof InternalAuthenticationServiceException) {
            resultData = Result.error(ResultCode.ERROR, "内部身份验证服务异常，登录失败！");
        } else {
            resultData = Result.error(ResultCode.ERROR, "登录失败！未知的认证异常：" + exception.getMessage());
        }
        //将结果转成json,调用工具类返回结果给前端
        WebUtils.renderString(response, JSON.toJSONString(resultData));
    }
}
