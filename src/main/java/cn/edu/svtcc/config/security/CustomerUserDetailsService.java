package cn.edu.svtcc.config.security;

import cn.edu.svtcc.exception.UserCountLockException;
import cn.edu.svtcc.sys.entity.User;
import cn.edu.svtcc.sys.service.IUserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 用户认证处理类
 *
 */
@Component
public class CustomerUserDetailsService implements UserDetailsService {
    @Resource
    private IUserService userService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUserByUserName(username);
        //对象为空则验证失败
        if (Objects.isNull(user)){
            throw new UsernameNotFoundException("用户名或密码错误！");
        }else if ("0".equals(user.getStatus())){
            throw new DisabledException("该用户账号被封禁，具体请联系管理员！");
        }

        //查询角色对应的权限信息返回
        // 格式ROLE_admin,ROLE_common,system:user:resetPwd,system:role:delete,system:user:list,system:menu:query,system:menu:list,system:menu:add,system:user:delete,system:role:list,system:role:menu,system:user:edit,system:user:query,system:role:edit,system:user:add,system:user:role,system:menu:delete,system:role:add,system:role:query,system:menu:edit
        user.setAuthorities(grantedAuthorityList(user.getUserId()));
        return user;
    }

    /**
     * 授予权限列表
     *
     * @param userId 用户 ID
     * @return {@link List}<{@link GrantedAuthority}>
     */
    private List<GrantedAuthority> grantedAuthorityList(Long userId){
        //根据用户id查询角色及对应的权限列表
        String authority = userService.getUserAuthorityById(userId);
        return AuthorityUtils.commaSeparatedStringToAuthorityList(authority);
    }
}
