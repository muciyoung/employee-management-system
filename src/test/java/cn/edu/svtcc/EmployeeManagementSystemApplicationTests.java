package cn.edu.svtcc;


import cn.edu.svtcc.sys.mapper.UserMapper;
import cn.edu.svtcc.utils.JwtUtil;
import cn.edu.svtcc.utils.KaptchaTextCreator;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
class EmployeeManagementSystemApplicationTests {
    @Resource
    private UserMapper userMapper;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private JwtUtil jwtUtil;

    @Test
    void contextLoads() {

    }

    /**
     * redis测试
     * @throws InterruptedException
     */
    @Test
    public void A() throws InterruptedException {
        //插入单条数据
        redisTemplate.opsForValue().set("key1", "我是新信息");
        System.out.println(redisTemplate.opsForValue().get("key1"));
        //插入单条数据（存在有效期）
        System.out.println("-----------------");
        redisTemplate.opsForValue().set("key2", "这是一条会过期的信息", 5, TimeUnit.SECONDS);//向redis里存入数据和设置缓存时间
        System.out.println(redisTemplate.hasKey("key2"));//检查key是否存在，返回boolean值
        System.out.println(redisTemplate.opsForValue().get("key2"));
        Thread.sleep(2000);
        System.out.println(redisTemplate.hasKey("key2"));//检查key是否存在，返回boolean值
        System.out.println(redisTemplate.opsForValue().get("key2"));
        System.out.println("-----------------");

    }

    @Test
    public void A1() {
        redisTemplate.delete("key01");
    }

    /**
     * 创建token测试
     */

    @Test
    public void test001(){
        KaptchaTextCreator kaptchaTextCreator = new KaptchaTextCreator();
        String text = kaptchaTextCreator.getText();
        System.out.println("text = " + text);
    }
}
